#!/usr/bin/env python

"""
Created on Fri Mar  1 10:45:56 2019

@author: mitchell
"""
import rospy
import std_msgs.msg
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge, CvBridgeError


class image_republish:
    def __init__(self):
        self.image_sub = rospy.Subscriber("/image_raw",
                                        Image, self.image_callback)

        self.img = Image()
        self.bridge = CvBridge()

        self.save_path = "/home/mitchell/NBL_images/"
        self.img_num = 0

    def image_callback(self, msg):
        try:
            img = self.bridge.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError as e:
          print(e)

        self.img_num += 1
        img_num = format(self.img_num, "05")

        img_name = self.save_path + img_num + ".png"

        cv2.imwrite(img_name, img)

if __name__ == "__main__":
    rospy.init_node("bag_to_png")
    IR = image_republish()
    rospy.spin()
