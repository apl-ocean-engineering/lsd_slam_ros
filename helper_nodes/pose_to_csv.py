#!/usr/bin/env python
from geometry_msgs.msg import PoseStamped
import rospy

class PoseCsv:
    def __init__(self):
        self.right_image_pub = rospy.Subscriber("/lsd_slam/pose", PoseStamped, self.pose_callback)
        
        save_path = "/home/mitchell/final_hmi_presentation/pose_output/pose.txt"
        
        self.save_file = open(save_path, "w+")
        self.count = 0
        self.time_init = 0.0
        
    def pose_callback(self, msg):
        if self.count == 0:
            self.time_init = msg.header.stamp.to_sec()
        self.count += 1
        t = msg.header.stamp.to_sec() - self.time_init
        print(t)
        x = msg.pose.position.x
        y = msg.pose.position.y
        z = msg.pose.position.z
        
        qx = msg.pose.orientation.x
        qy = msg.pose.orientation.y
        qz = msg.pose.orientation.z
        qw = msg.pose.orientation.w
        
        write_line = str(t) + " " + str(x) + " " + str(y) + " " + str(z) + " " + str(qx) + " " + str(qy) + " " + str(qz) + " " + str(qw) + "\n"
        
        self.save_file.write(write_line)












if __name__ == '__main__':
    rospy.init_node("pose_to_csv")
    PC = PoseCsv()
    rospy.spin()
