import argparse
import rospy
from lsd_slam_msgs.msg import frameMsg, framePoseData
from geometry_msgs.msg import PoseStamped
import datetime


class PoseRecorder:
    def __init__(self, save_folder):
        self.current_frame_save_file = open(
                 save_folder + str(datetime.datetime.now()) + 'current_frame.txt', 'a+')
        self.frame_poses_save_file = open(
                 save_folder + str(datetime.datetime.now()) + 'frame_poses.txt', 'a+')
        self.live_poses_save_file = open(
                 save_folder + str(datetime.datetime.now()) + 'current_pose.txt', 'a+')
        self.save_folder = save_folder

        rospy.Subscriber(
            "lsd_slam/frames", frameMsg, self.current_frame_callback)
        rospy.Subscriber(
            "lsd_slam/pose", PoseStamped, self.pose_callback)
        rospy.Subscriber(
            "lsd_slam/framePoses", framePoseData, self.frame_poses_callback)

        rospy.spin()

    def pose_callback(self, msg):
        current_secs = msg.header.stamp.secs
        current_nsecs = msg.header.stamp.nsecs
        id = msg.header.seq
        q_x = msg.pose.orientation.x
        q_y = msg.pose.orientation.y
        q_z = msg.pose.orientation.z
        q_w = msg.pose.orientation.w

        x = msg.pose.position.x
        y = msg.pose.position.y
        z = msg.pose.position.z

        frame_toKf = (q_x, q_y, q_z, q_w, x, y, z)

        save_line = str(current_secs) + "," + str(current_nsecs) + "," + \
            str(id) + "," + str(frame_toKf)

        self.live_poses_save_file.write(save_line + '\n')

    def current_frame_callback(self, msg):
        current_secs = msg.header.stamp.secs
        current_nsecs = msg.header.stamp.nsecs
        id = msg.id
        frame_toKf = msg.frameToKF
        kf_id = msg.kf.id
        kf_to_world = msg.kf.camToWorld

        save_line = str(current_secs) + "," + str(current_nsecs) + "," + \
            str(id) + "," + str(frame_toKf) + "," + str(kf_id) + "," + \
            str(kf_to_world)
        self.current_frame_save_file.write(save_line + '\n')

    def frame_poses_callback(self, msg):
        current_secs = msg.header.stamp.secs
        current_nsecs = msg.header.stamp.nsecs
        id_data = msg.id
        fp_data = msg.frameData

        save_line = str(current_secs) + "," + str(current_nsecs) + "," + \
            str(id_data) + "," + str(fp_data)

        self.frame_poses_save_file.write(save_line + '\n')

    def __del__(self):
        self.current_frame_save_file.close()
        self.frame_poses_save_file.close()


if __name__ == '__main__':
    rospy.init_node("pose_recorder")
    parser = argparse.ArgumentParser("Record LSD-SLAM pose infmormation")
    parser.add_argument("save_folder")

    args = parser.parse_args()
    PR = PoseRecorder(args.save_folder)
