#!/usr/bin/env python

"""
pointgrey_camera_driver (at least the version installed with apt-get) doesn't
properly handle camera info in indigo.
This node is a work-around that will read in a camera calibration .yaml
file (as created by the cameracalibrator.py in the camera_calibration pkg),
convert it to a valid sensor_msgs/CameraInfo message, and publish it on a
topic.
The yaml parsing is courtesy ROS-user Stephan:
    http://answers.ros.org/question/33929/camera-calibration-parser-in-python/
This file just extends that parser into a rosnode.

from: https://gist.github.com/rossbar/ebb282c3b73c41c1404123de6cea4771
"""


import rospy
import yaml
from sensor_msgs.msg import CameraInfo
import os
import std_msgs
from os.path import abspath, dirname
import sys


def yaml_to_CameraInfo(yaml_fname, frame):
    """
    Parse a yaml file containing camera calibration data (as produced by
    rosrun camera_calibration cameracalibrator.py) into a
    sensor_msgs/CameraInfo msg.

    Parameters
    ----------
    yaml_fname : str
        Path to yaml file containing camera calibration data
    Returns
    -------
    camera_info_msg : sensor_msgs.msg.CameraInfo
        A sensor_msgs.msg.CameraInfo message containing the camera calibration
        data
    """
    # Load data from file
    with open(yaml_fname, "r") as file_handle:
        calib_data = yaml.load(file_handle)
    # Parse
    header = std_msgs.msg.Header()
    camera_info_msg = CameraInfo()
    camera_info_msg.header = header
    camera_info_msg.header.frame_id = frame
    camera_info_msg.width = calib_data["image_width"]
    camera_info_msg.height = calib_data["image_height"]
    camera_info_msg.K = calib_data["camera_matrix"]["data"]
    camera_info_msg.D = calib_data["distortion_coefficients"]["data"]
    camera_info_msg.R = calib_data["rectification_matrix"]["data"]
    camera_info_msg.P = calib_data["projection_matrix"]["data"]
    camera_info_msg.distortion_model = calib_data["distortion_model"]
    return camera_info_msg


if __name__ == "__main__":
    name = "info_pub"
    rospy.init_node(name)

    ns = sys.argv[1]
    frame = sys.argv[2]
    if frame[0] != '/':
        frame = '/' + frame

    print(frame)
    if frame == "/camera_left":
        namespace = ns + "/left_info"
    elif frame == "/camera_right":
        namespace = ns + "/right_info"
    else:
        rospy.logerr("No valid frame given")
    rospy.logwarn("Info pub")
    path = abspath(os.path.join(dirname(__file__), ".."))

    print(namespace)
    if rospy.has_param(namespace):

        full_info = path + "/params/camera_calibrations/" + \
            rospy.get_param(namespace) + ".yaml"

        log = "Use camera info: /params/camera_calibrations/" + \
            str(rospy.get_param(namespace)) + ".yaml"
        rospy.loginfo(log)
    else:
        rospy.logwarn("No camera info specified")
        exit()

    msg = yaml_to_CameraInfo(full_info, frame)
    # Initialize publisher node
    if frame == "/camera_left":
        publisher = rospy.Publisher(
            "/camera/left/camera_info_lag", CameraInfo, queue_size=10)
    elif frame == "/camera_right":
        publisher = rospy.Publisher(
            "/camera/right/camera_info_lag", CameraInfo, queue_size=10)

    rate = rospy.Rate(30)
    # Run publisher
    while not rospy.is_shutdown():
        publisher.publish(msg)
        rate.sleep()
