#!/usr/bin/env python
"""
Created on Wed Feb 27 12:32:07 2019

@author: mitchell
"""

import rospy
from sensor_msgs.msg import CameraInfo
import numpy as np


class InfoResize:
    def __init__(self, downsample, im_size):
        self.im_size = im_size
        self.downsample = downsample

        self.left_info_pub = rospy.Publisher(
            "/camera/left/downsample_camera_info", CameraInfo, queue_size=1)
        self.right_info_pub = rospy.Publisher(
            "/camera/right/downsample_camera_info", CameraInfo, queue_size=1)

        rospy.Subscriber("/camera/left/camera_info",
                         CameraInfo, self.left_full_info_callback)
        rospy.Subscriber("/camera/right/camera_info",
                         CameraInfo, self.right_full_info_callback)

        self.right_camInfo = CameraInfo()
        self.left_camInfo = CameraInfo()

    def downsample_info(self, info):
        downsampleInfoLeft = CameraInfo()
        downsampleInfoLeft.header = info.header

        downsampleInfoLeft.width = info.width/self.downsample
        downsampleInfoLeft.height = info.height/self.downsample
        downsampleInfoLeft.distortion_model = info.distortion_model
        downsampleInfoLeft.D = info.D
        downsampleInfoLeft.K = np.divide(
            np.array(info.K), self.downsample)
        downsampleInfoLeft.R = info.R
        downsampleInfoLeft.P = np.divide(
            np.array(info.P), self.downsample)

        return downsampleInfoLeft

    def right_full_info_callback(self, msg):
        self.right_camInfo = msg

    def left_full_info_callback(self, msg):
        self.left_camInfo = msg

        downsample_left_info = self.downsample_info(self.left_camInfo)
        downsample_right_info = self.downsample_info(self.right_camInfo)

        self.left_info_pub.publish(downsample_left_info)
        self.right_info_pub.publish(downsample_right_info)


if __name__ == '__main__':
    namespace = 'info_resize'
    rospy.init_node(namespace)
    img_width = rospy.get_param(
        rospy.resolve_name("/lsd_slam/input_image/width"))
    img_height = rospy.get_param(
        rospy.resolve_name("/lsd_slam/input_image/height"))
    downsample = rospy.get_param(
        rospy.resolve_name("/lsd_slam/input_image/decimation"))
    IR = InfoResize(downsample, (img_width, img_height))
    rospy.spin()
