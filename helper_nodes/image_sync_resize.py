#!/usr/bin/env python
"""
Created on Wed Feb 27 12:32:07 2019

@author: mitchell
"""

import rospy
import std_msgs.msg
import numpy as np
from sensor_msgs.msg import Image, CameraInfo
from cv_bridge import CvBridge, CvBridgeError
import cv2
import copy
import message_filters
import sys


class ImageRepublish:
    def __init__(self, downsample, im_size, sharpen=False, flip_img=False):
        self.im_size = im_size
        self.downsample = downsample
        self.sharpen = sharpen

        self.right_image_pub = rospy.Publisher(
            "/camera/right/image_raw", Image, queue_size=1)
        self.left_image_pub = rospy.Publisher(
            "/camera/left/image_raw", Image, queue_size=1)

        self.right_info_pub = rospy.Publisher(
            "/camera/right/downsample_camera_info", CameraInfo, queue_size=1)
        self.left_info_pub = rospy.Publisher(
            "/camera/left/downsample_camera_info", CameraInfo, queue_size=1)

        self.right_full_info_pub = rospy.Publisher(
            "/camera/right/camera_info", CameraInfo, queue_size = 1)
        self.left_full_info_pub = rospy.Publisher(
            "/camera/left/camera_info", CameraInfo, queue_size = 1)

        left_sub = message_filters.Subscriber(
            '/camera/left/image_raw_lag', Image)
        right_sub = message_filters.Subscriber(
            '/camera/right/image_raw_lag', Image)
        #
        # left_sub = message_filters.Subscriber(
        #     '/head_stereo/left/image_raw', Image)
        # right_sub = message_filters.Subscriber(
        #     '/head_stereo/right/image_raw', Image)

        ts = message_filters.ApproximateTimeSynchronizer(
            [left_sub, right_sub],
            10, 0.5, allow_headerless=True)
        ts.registerCallback(self.image_sync_callback)

        rospy.Subscriber("/camera/right/camera_info_lag",
                         CameraInfo, self.right_full_info_callback)
        rospy.Subscriber("/camera/left/camera_info_lag",
                         CameraInfo, self.left_full_info_callback)

        self.right_img = Image()
        self.left_img = Image()
        self.right_camInfo = CameraInfo()
        self.left_camInfo = CameraInfo()
        self.right_full_camInfo = CameraInfo()
        self.left_full_camInfo = CameraInfo()

        self.flip_img = flip_img

        self.bridge = CvBridge()

    def parse_image(self, img_msg):
        cv_img = self.bridge.imgmsg_to_cv2(img_msg, "mono8")
        if (img_msg.width, img_msg.height) != self.im_size:
            try:
                cv_img = cv2.resize(cv_img, self.im_size)
            except CvBridgeError as e:
                print(e)
        if self.sharpen:
            pass
            #kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
            kernel = -1*np.ones((3,3))
            kernel[2,2] = 9
            cv_img = cv2.filter2D(cv_img, -1, kernel)
            cv_img = cv2.medianBlur(cv_img,5)
            # cv_img = cv2.normalize(
            #     cv_img, None, alpha=0, beta=1,
            #     norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
        #cv_img = np.uint8(cv_img)
        if self.flip_img:
            print('flip')
            cv_img = cv2.flip(cv_img, -1)
        img = self.bridge.cv2_to_imgmsg(cv_img, "mono8")
        return img

    def downsample_info(self, info):
        downsampleInfoLeft = CameraInfo()
        downsampleInfoLeft.header = info.header

        downsampleInfoLeft.width = info.width/self.downsample
        downsampleInfoLeft.height = info.height/self.downsample
        downsampleInfoLeft.distortion_model = info.distortion_model
        downsampleInfoLeft.D = info.D
        downsampleInfoLeft.K = np.divide(
            np.array(info.K), self.downsample)
        downsampleInfoLeft.R = info.R
        downsampleInfoLeft.P = np.divide(
            np.array(info.P), self.downsample)

        return downsampleInfoLeft

    def image_sync_callback(self, left_msg, right_msg):
        left_img = self.parse_image(left_msg)
        right_img = self.parse_image(right_msg)

        right_full_camInfo = self.right_full_camInfo
        left_full_camInfo = self.left_full_camInfo

        right_h = std_msgs.msg.Header()
        right_h.stamp = rospy.Time.now()
        right_h.frame_id = "camera_right"

        left_h = copy.deepcopy(right_h)
        left_h.frame_id = "camera_left"

        right_img.header = right_h

        left_img.header = left_h
        #right_camInfo.header = right_h

        #left_camInfo.header = left_h
        right_full_camInfo.header = right_h
        left_full_camInfo.header = left_h

        downsample_left_info = self.downsample_info(left_full_camInfo)
        downsample_right_info = self.downsample_info(right_full_camInfo)

        self.right_image_pub.publish(right_img)
        self.left_image_pub.publish(left_img)
        self.right_full_info_pub.publish(right_full_camInfo)
        self.left_full_info_pub.publish(left_full_camInfo)

        self.right_info_pub.publish(downsample_right_info)
        self.left_info_pub.publish(downsample_left_info)

    def right_full_info_callback(self, msg):
        self.right_full_camInfo = msg

    def left_full_info_callback(self, msg):
        self.left_full_camInfo = msg

    def run(self):
        r = rospy.Rate(10)
        while not rospy.is_shutdown():
            r.sleep()


if __name__ == '__main__':
    namespace = 'image_sync_resize'
    if len(sys.argv) > 3:
        ns = sys.argv[1]

    else:
        ns = "/lsd_slam"

    rospy.init_node(namespace)

    img_width = rospy.get_param(
        rospy.resolve_name(ns + "/input_image/width"))
    img_height = rospy.get_param(
        rospy.resolve_name(ns + "/input_image/height"))
    downsample = rospy.get_param(
        rospy.resolve_name(ns + "/input_image/decimation"))
    if rospy.has_param(ns + "/slam_image_processing/sharpen/do_sharpen"):
        sharpen = rospy.get_param(
            rospy.resolve_name(ns + "/slam_image_processing/sharpen/do_sharpen"))
        if sharpen:
            rospy.loginfo("Sharpening image")
        else:
            rospy.loginfo("Not sharpening image")
    else:
        sharpen = False
        rospy.loginfo("Not sharpening image")
    print("downsample", downsample)
    flip_img = rospy.get_param("/image_sync_resize/flip_img")
    IR = ImageRepublish(downsample, (img_width, img_height),
        sharpen=sharpen, flip_img=flip_img)
    rospy.spin()
