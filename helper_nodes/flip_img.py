#!/usr/bin/env python

from cv_bridge import CvBridge
from sensor_msgs.msg import Image
import cv2
import rospy

publisher = rospy.Publisher('/image_rotate', Image, queue_size=10)
bridge = CvBridge()

def callback(msg):
    img = bridge.imgmsg_to_cv2(msg, "rgb8")
    img = cv2.flip(img, -1)
    img_msg = bridge.cv2_to_imgmsg(img, "rgb8")
    img_msg.header = msg.header
    publisher.publish(img_msg)

if __name__ == '__main__':
    rospy.init_node('flip_image')
    rospy.Subscriber('/image_raw', Image, callback)

    rospy.spin()
