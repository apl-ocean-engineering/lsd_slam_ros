#!/usr/bin/env python
"""
Created on Wed Feb 27 12:32:07 2019

@author: mitchell
"""

import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2

class image_republish:
    def __init__(self):
        
        self.right_image_pub = rospy.Publisher("/camera/right/image_raw",Image, queue_size=1)
        self.left_image_pub = rospy.Publisher("/camera/left/image_raw",Image, queue_size=1)           
        
        rospy.Subscriber("/camera/right/image_raw_switch",Image,self.right_image_callback)
        rospy.Subscriber("/camera/left/image_raw_switch",Image,self.left_image_callback)        
        

        self.right_img = Image()     
        self.left_img = Image()
        
        self.bridge=CvBridge()
  

        
    def right_image_callback(self, msg):
        self.right_img = msg
       
        
        
    def left_image_callback(self, msg):
        self.left_img = msg
        
        right_img = self.right_img
        left_img = self.left_img
        right_header = right_img.header
        left_header = left_img.header
        
        try:
            left_cv_img = self.bridge.imgmsg_to_cv2(left_img, "mono8")        
            right_cv_img = self.bridge.imgmsg_to_cv2(right_img, "mono8")
            
            right_cv_img = cv2.flip(right_cv_img, -1) 
            left_cv_img = cv2.flip(left_cv_img, -1) 

            right_img = self.bridge.cv2_to_imgmsg(left_cv_img, "mono8")
            left_img = self.bridge.cv2_to_imgmsg(right_cv_img, "mono8")
            right_img.header = right_header
            left_img.header = left_header
 
        except CvBridgeError as e:
          print(e)
        
        self.right_image_pub.publish(right_img)    
        self.left_image_pub.publish(left_img)



    def run(self):
        r = rospy.Rate(10)
        i = 0
        while not rospy.is_shutdown():
            i+=1
            r.sleep()

if __name__ == '__main__':
    rospy.init_node("image_switch")
    IR = image_republish()
    rospy.spin()
