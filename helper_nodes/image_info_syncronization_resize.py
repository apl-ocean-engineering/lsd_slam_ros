#!/usr/bin/env python
"""
Created on Wed Feb 27 12:32:07 2019

@author: mitchell
"""

import rospy
import std_msgs.msg
import numpy as np
from sensor_msgs.msg import Image, CameraInfo, PointCloud2
from cv_bridge import CvBridge, CvBridgeError
import cv2
import copy

class image_republish:
    def __init__(self):

        self.right_image_pub = rospy.Publisher("/camera/right/image_raw",Image, queue_size=1)
        self.left_image_pub = rospy.Publisher("/camera/left/image_raw",Image, queue_size=1)
        self.right_info_pub = rospy.Publisher("/camera/right/downsample_camera_info", CameraInfo, queue_size = 1)
        self.left_info_pub = rospy.Publisher("/camera/left/downsample_camera_info", CameraInfo, queue_size = 1)
        self.right_full_info_pub = rospy.Publisher("/camera/right/camera_info", CameraInfo, queue_size = 1)
        self.left_full_info_pub = rospy.Publisher("/camera/left/camera_info", CameraInfo, queue_size = 1)

        self.seikowave_pub = rospy.Publisher("/seikowave_node/cloud",PointCloud2, queue_size=1)
        """
        self.right_image_pub = rospy.Publisher("/stereo/right/image_raw",Image, queue_size=1)
        self.right_info_pub = rospy.Publisher("/stereo/right/camera_info", CameraInfo, queue_size = 1)
        self.left_image_pub = rospy.Publisher("/stereo/left/image_raw",Image, queue_size=1)
        self.left_info_pub = rospy.Publisher("/stereo/left/camera_info", CameraInfo, queue_size = 1)
        """
        rospy.Subscriber("/camera/right/image_raw_lag",Image,self.right_image_callback)
        rospy.Subscriber("/camera/left/image_raw_lag",Image,self.left_image_callback)
        rospy.Subscriber("/camera/right/camera_info_lag",CameraInfo,self.right_info_callback)
        rospy.Subscriber("/camera/left/camera_info_lag",CameraInfo,self.left_info_callback)
        rospy.Subscriber("/camera/right/full_camera_info_lag",CameraInfo,self.right_full_info_callback)
        rospy.Subscriber("/camera/left/full_camera_info_lag",CameraInfo,self.left_full_info_callback)

        rospy.Subscriber("/seikowave_node/cloud_lag",PointCloud2, self.seikowave_callback)

        rospy.Subscriber("/camera/left/image_raw",Image,self.current_time)

        self.right_img = Image()
        self.left_img = Image()
        self.right_camInfo = CameraInfo()
        self.left_camInfo = CameraInfo()
        self.right_full_camInfo = CameraInfo()
        self.left_full_camInfo = CameraInfo()

        self.bridge = CvBridge()

        self.seikowave = PointCloud2()

        self.time_now = rospy.Time.now()

    def current_time(self, msg):
        time_now = msg.header.stamp
        self.time_now = time_now

        seikowave = self.seikowave

        header = std_msgs.msg.Header()
        #header.stamp = rospy.Time.now()
        header.stamp = time_now
        header.frame_id = seikowave.header.frame_id
        seikowave.header = header

        self.seikowave = self.seikowave


        #self.seikowave_pub.publish(seikowave)

    def seikowave_callback(self, msg):
        self.seikowave = msg
        seikowave = self.seikowave
        header = std_msgs.msg.Header()
        header.stamp = self.time_now
        header.frame_id = seikowave.header.frame_id
        seikowave.header = header
        self.seikowave_pub.publish(seikowave)
        #self.seikowave_pub.publish(seikowave)

    def right_image_callback(self, msg):
        """
        try:
            cv_img = self.bridge.imgmsg_to_cv2(msg, "bgr8")
            #cv_img = np.uint8(cv_img)
            #cv2.imshow("img", cv_img)
            #cv2.waitKey(1)
            self.right_img = self.bridge.cv2_to_imgmsg(cv_img, "bgr8")
        except CvBridgeError as e:
            print(e)
        """
        #self.right_image_pub.publish(msg)
        self.right_img = msg

    def right_info_callback(self, msg):
        #self.right_full_info_pub.publish(msg)
        self.right_camInfo = msg

    def left_image_callback(self, msg):
        #self.left_image_pub.publish(msg)
        self.left_img = msg

        right_img = self.right_img
        """
        try:
            cv_img = self.bridge.imgmsg_to_cv2(msg, "mono8")
            cv_img = np.uint8(cv_img)
            #cv2.imshow("img", cv_img)
            #cv2.waitKey(1)
            right_img = self.bridge.cv2_to_imgmsg(cv_img, "mono8")
        except CvBridgeError as e:
            print(e)
        """
        left_img = self.left_img

        #seikowave = self.seikowave


        #CONVERT IMAGES

        """
        try:
            cv_img = self.bridge.imgmsg_to_cv2(right_img, "bgr8")
            right_cv_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2GRAY)
            right_img = self.bridge.cv2_to_imgmsg(right_cv_image, "mono8")
            #print('here')
            #left_cv_image = self.bridge.imgmsg_to_cv2(left_img, left_img.encoding)
            #right_cv_image = cv2.flip(right_cv_image, -1)
            #left_cv_image = cv2.flip(left_cv_image, -1)
        except CvBridgeError as e:
          print(e)
         """

        right_camInfo = self.right_camInfo
        left_camInfo = self.left_camInfo
        right_full_camInfo = self.right_full_camInfo
        left_full_camInfo = self.left_full_camInfo





        right_h = std_msgs.msg.Header()
        right_h.stamp = rospy.Time.now()
        right_h.frame_id = self.right_img.header.frame_id

        left_h = copy.deepcopy(right_h)
        left_h.frame_id = self.left_img.header.frame_id

        right_img.header = right_h

        left_img.header = left_h
        right_camInfo.header = right_h

        left_camInfo.header = left_h
        right_full_camInfo.header = right_h
        left_full_camInfo.header = left_h




        self.right_image_pub.publish(right_img)
        self.left_image_pub.publish(left_img)
        self.right_info_pub.publish(right_camInfo)
        self.left_info_pub.publish(left_camInfo)
        self.right_full_info_pub.publish(right_full_camInfo)
        self.left_full_info_pub.publish(left_full_camInfo)


    def left_info_callback(self, msg):
        #self.left_full_info_pub.publish(msg)
        self.left_camInfo = msg

    def right_full_info_callback(self, msg):
        self.right_full_camInfo = msg


    def left_full_info_callback(self, msg):
        self.left_full_camInfo = msg



    def run(self):
        r = rospy.Rate(10)
        i = 0
        while not rospy.is_shutdown():
            i+=1
            r.sleep()

if __name__ == '__main__':
    rospy.init_node("image_sync")
    IR = image_republish()
    rospy.spin()
