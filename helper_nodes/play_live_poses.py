import rospy
from geometry_msgs.msg import PoseStamped
import argparse

class PosePlayer:
    def __init__(self, name):
        self.name = name

        self.pose_publisher = rospy.Publisher(
                '/playback/pose', PoseStamped, queue_size=10)

    def run(self):
        f = open(self.name, 'r')
        loop_rate = rospy.Rate(7)
        for line in f:
            if rospy.is_shutdown():
                exit()
            data = line.split(',')
            current_secs = data[0]
            current_nsecds = data[1]
            id = data[2]
            frame_toKf = []
            for val in data[3:]:
                val = float(val.replace('(', '').replace(')', '').rstrip())
                frame_toKf.append(val)

            poseStamped = PoseStamped()
            poseStamped.header.stamp = rospy.Time.now()
            poseStamped.header.frame_id = "map"
            poseStamped.pose.orientation.x = frame_toKf[0]
            poseStamped.pose.orientation.y = frame_toKf[1]
            poseStamped.pose.orientation.z = frame_toKf[2]
            poseStamped.pose.orientation.w = frame_toKf[3]

            poseStamped.pose.position.x = frame_toKf[4]
            poseStamped.pose.position.y = frame_toKf[5]
            poseStamped.pose.position.z = frame_toKf[6]

            self.pose_publisher.publish(poseStamped)

            loop_rate.sleep()




if __name__ == '__main__':
    rospy.init_node("pose_player")
    parser = argparse.ArgumentParser("Record LSD-SLAM pose infmormation")
    parser.add_argument("playback_file")

    args = parser.parse_args()
    posePlayer = PosePlayer(args.playback_file)

    posePlayer.run()
