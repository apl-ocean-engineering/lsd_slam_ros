#!/usr/bin/env python
"""
Created on Tue May 14 13:40:14 2019

@author: mitchell
"""


import std_msgs
import rospy
from sensor_msgs.msg import CameraInfo
import numpy as np


class DownsampleInfo:
    def __init__(self, downsample):
        self.downsampleInfoLeft = CameraInfo()
        self.downsampleInfoRight = CameraInfo()

        self.downsample = downsample

        rospy.Subscriber(
            '/camera/left/camera_info', CameraInfo,
            self.left_info_callback)
        rospy.Subscriber(
            '/camera/right/camera_info', CameraInfo,
            self.right_info_callback)

        self.left_pub = rospy.Publisher(
            '/camera/left/downsample_camera_info',
            CameraInfo, queue_size=1)
        self.right_pub = rospy.Publisher(
            '/camera/right/downsample_camera_info',
            CameraInfo, queue_size=1)

    def left_info_callback(self, msg):
        header = std_msgs.msg.Header()
        self.downsampleInfoLeft.header = header

        self.downsampleInfoLeft.width = msg.width/self.downsample
        self.downsampleInfoLeft.height = msg.height/self.downsample
        self.downsampleInfoLeft.distortion_model = msg.distortion_model
        self.downsampleInfoLeft.D = msg.D
        self.downsampleInfoLeft.K = np.divide(np.array(msg.K), self.downsample)
        self.downsampleInfoLeft.R = msg.R
        self.downsampleInfoLeft.P = np.divide(np.array(msg.P), self.downsample)

        self.left_pub.publish(self.downsampleInfoLeft)

    def right_info_callback(self, msg):
        header = std_msgs.msg.Header()
        self.downsampleInfoRight.header = header

        self.downsampleInfoRight.width = msg.width/self.downsample
        self.downsampleInfoRight.height = msg.height/self.downsample
        self.downsampleInfoRight.distortion_model = msg.distortion_model
        self.downsampleInfoRight.D = msg.D
        self.downsampleInfoRight.K = np.divide(np.array(msg.K), self.downsample)
        self.downsampleInfoRight.R = msg.R
        self.downsampleInfoRight.P = np.divide(np.array(msg.P), self.downsample)

        self.right_pub.publish(self.downsampleInfoRight)


if __name__ == '__main__':
    namespace = 'downsample_info'
    rospy.init_node(namespace)
    downsample = rospy.get_param(
        rospy.resolve_name(namespace + "/downsample"))
    DI = DownsampleInfo(downsample)
    rospy.spin()
