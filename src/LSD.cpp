/**
 *  This version "LSD" of the main binary has no GUI dependencies.
 *  Pangolin, etc are not required to build this version.
 *  (of course, it's not that exciting to watch!)
 *
 *  See LSD_GUI which is functionally identical but does have the
 *  Pangolin-based GUI
 *
 * Based on original LSD-SLAM code from:
 * Copyright 2013 Jakob Engel <engelj at in dot tum dot de> (Technical
 * University of Munich) For more information see
 * <http://vision.in.tum.de/lsdslam>
 *
 * LSD-SLAM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LSD-SLAM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LSD-SLAM. If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/thread.hpp>
#include <fstream>

#include "g3_to_ros_logger/ROSLogSink.h"
#include "g3_to_ros_logger/g3logger.h"

#include "SlamSystem.h"

#include "util/settings.h"

#include <ros/package.h>
#include <ros/ros.h>

#include "util/Configuration.h"
#include "util/ThreadMutexObject.h"
#include "util/globalFuncs.h"

#include "lsd_slam_ros/ROSInputThread.h"
#include "lsd_slam_ros/ROSOutput3DWrapper.h"

#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/synchronizer.h>
#include <message_filters/time_synchronizer.h>

using namespace lsd_slam;

typedef message_filters::sync_policies::ApproximateTime<
				sensor_msgs::Image, slam_msgs::disparityIdepthMsg>
        SyncPolicy;

void getROSParams(ros::NodeHandle nh_) {
		// Using camelCase for c++ params, and underscore for ros params.

		std::string lsd_slam_frame, global_frame;
		if (nh_.getParam("LSD_SLAM_frame", lsd_slam_frame)) {
				ROS_INFO("Seeting frame %s as lsd_slam output frame",
				         lsd_slam_frame.c_str());
		} else {
				ROS_INFO("Setting lsd_slam output frame to camera_left");
				lsd_slam_frame = "camera_left";
		}

		Conf().lsdFrame = lsd_slam_frame;

		if (nh_.getParam("global_frame", global_frame)) {
				ROS_INFO("Seeting frame %s as global_frame", global_frame.c_str());
		} else {
				ROS_INFO("Setting global frame to map");
				global_frame = "map";
		}

		Conf().globalFrame = global_frame;

		bool do_stereo;
		if (nh_.getParam("do_stereo", do_stereo)) {
				if (do_stereo) {
						ROS_INFO("Doing stereo LSD-SLAM");
				} else {
						ROS_INFO("Not doing stereo LSD-SLAM");
				}
		} else {
				ROS_INFO("Doing stereo LSD-SLAM");
				do_stereo = true;
		}

		Conf().doStereo = do_stereo;

		bool use_Ekf;
		if (nh_.getParam("do_EKF", use_Ekf)) {
				ROS_INFO("Decision to do EKF : %i", use_Ekf);
		} else {
				ROS_INFO("Not using EKF");
				use_Ekf = false;
		}

		Conf().useEkf = use_Ekf;

		bool publish_set_pose;
		if (nh_.getParam("publish_set_pose", publish_set_pose)) {
				ROS_INFO("Decision to publish set_pose : %i", publish_set_pose);
		} else {
				ROS_INFO("Not publishing set_pose ");
				publish_set_pose = false;
		}

		Conf().publishSetPose = publish_set_pose;

		int ref_frame;
		if (nh_.getParam("lsd_slam_processing/reference_image", ref_frame)) {
				ROS_INFO("Subscribing to topic : %i for left camera image", ref_frame);
		} else {
				ROS_WARN(
						"No refrence frame specified, setting refrence to left image topic");
				ref_frame = 0;
		}

		Conf().refFrame = ref_frame;

		int slam_image_width, slam_image_height, decimation;

		if (nh_.getParam("input_image/width", slam_image_width) &&
		    nh_.getParam("input_image/height", slam_image_height)) {
				ROS_INFO_STREAM("Slam image size is " << slam_image_width << " x "
				                                      << slam_image_height);
		} else {
				ROS_FATAL("No image width or height defined");
		}
		if (nh_.getParam("input_image/decimation", decimation)) {
				ROS_INFO("Decimation : %i ", decimation);
		} else {
				ROS_WARN("No Decimation specified, setting to 1");
				decimation = 1;
		}

		slam_image_width /= decimation;
		slam_image_height /= decimation;

		CHECK(slam_image_width > 0 && slam_image_height > 0)
		        << "Slam height or width is zero";

		const ImageSize slamImageSize(slam_image_width, slam_image_height);

		// Load and set the configuration object
		Conf().setSlamImageSize(slamImageSize);

		float min_virtual_baseline_length;
		if (nh_.getParam("lsd_slam_processing/min_virtual_baseline_length",
		                 min_virtual_baseline_length)) {
				ROS_INFO("Min temporal SLAM speed : %f", min_virtual_baseline_length);
		} else {
				ROS_WARN("No minimium virtual baseline specified, defaulting to 0.1");
				min_virtual_baseline_length = 0.1;
		}

		Conf().minVirtualBaselineLength = min_virtual_baseline_length;

		bool suppress_lsd_points;
		if (nh_.getParam("lsd_slam_processing/suppress_LSD_points",
		                 suppress_lsd_points)) {
				ROS_INFO("Decision to suppress LSD points : %i", suppress_lsd_points);
		} else {
				ROS_INFO("Not suppressing LSD point generation");
				suppress_lsd_points = false;
		}

		bool sync_disparity_image;
		if (nh_.getParam("lsd_slam_processing/sync_disparity_image",
		                 sync_disparity_image)) {
				ROS_INFO("Decision to sync disparity : %i", sync_disparity_image);
		} else {
				ROS_INFO("Syncing disparity");
				sync_disparity_image = true;
		}

		Conf().syncDisparityImage = sync_disparity_image;


		float initalization_phase_count;
		if (nh_.getParam("lsd_slam_processing/initalization_phase_count", initalization_phase_count)) {

				ROS_INFO("initalization_phase_count : %f", initalization_phase_count);
		} else {
				ROS_INFO("initalization_phase_count set to 5.0");
				initalization_phase_count = 5.0;
		}
		Conf().initalizationPhaseCount = initalization_phase_count;

		Conf().suppressLSDPoints = suppress_lsd_points;

		bool display_depth_map;
		if (nh_.getParam("lsd_slam_debugging/display_depth_map", display_depth_map)) {
				ROS_INFO("Decision to display depth map : %i", display_depth_map);
		} else {
				ROS_INFO("Not displaying depth map");
				display_depth_map = false;
		}


		Conf().displayDepthMap = display_depth_map;

		bool display_gradient_map;
		if (nh_.getParam("lsd_slam_debugging/display_gradient_map",
		                 display_gradient_map)) {
				ROS_INFO("Decision to display gradient map : %i", display_gradient_map);
		} else {
				ROS_INFO("Not displaying gradient map");
				display_gradient_map = false;
		}

		Conf().displayGradientMap = display_gradient_map;

		bool display_input_fused_image;
		if (nh_.getParam("lsd_slam_debugging/display_input_fused_image",
		                 display_input_fused_image)) {
				ROS_INFO("Decision to display fused input image : %i",
				         display_input_fused_image);
		} else {
				ROS_INFO("Not displaying fused input image");
				display_input_fused_image = false;
		}

		Conf().displayInputFusedImage = display_input_fused_image;

		bool display_input_image;
		if (nh_.getParam("lsd_slam_debugging/display_input_image",
		                 display_input_image)) {
				ROS_INFO("Decision to display input image : %i", display_input_image);
		} else {
				ROS_INFO("Not displaying input image");
				display_input_image = false;
		}

		Conf().displayInputImage = display_input_image;

		bool print_optimization_info;
		if (nh_.getParam("lsd_slam_debugging/print_optimization_info",
		                 print_optimization_info)) {
				ROS_INFO("Decision to print optimization info : %i",
				         print_optimization_info);
		} else {
				ROS_INFO("Printing optimization info ");
				print_optimization_info = true;
		}

		Conf().print.optimizationInfo = print_optimization_info;

		bool print_constraint_info;
		if (nh_.getParam("lsd_slam_debugging/print_constraint_info",
		                 print_constraint_info)) {
				ROS_INFO("Decision to print constraint info : %i", print_constraint_info);
		} else {
				ROS_INFO("Printing constraint info ");
				print_constraint_info = true;
		}

		Conf().print.constraintSearchInfo = print_constraint_info;

		bool print_threading_info;
		if (nh_.getParam("lsd_slam_debugging/print_threading_info",
		                 print_threading_info)) {
				ROS_INFO("Decision to print constraint info : %i", print_threading_info);
		} else {
				ROS_INFO("Printing threading info ");
				print_threading_info = true;
		}

		Conf().print.threadingInfo = print_threading_info;

		// Check for image saturation
		bool image_saturation = false;
		double saturation_alpha;
		int saturation_beta;
		nh_.getParam("slam_image_processing/image_saturation", image_saturation);
		if (image_saturation) {
				ROS_INFO("Will be doing image saturation");
				if (nh_.getParam("slam_image_processing/saturation_alpha",
				                 saturation_alpha)) {

				} else {
						ROS_WARN("SATURATION NOT SPECIFIED");
						saturation_alpha = 1.0;
				}
				if (nh_.getParam("slam_image_processing/saturation_beta",
				                 saturation_beta)) {
				} else {
						saturation_beta = 1.0;
				}
				ROS_INFO_STREAM("Image saturation alpha: " << saturation_alpha
				                                           << " Image saturation beta: "
				                                           << saturation_beta);

				Conf().doImageSaturation = true;
				Conf().saturationAlpha = saturation_alpha;
				Conf().saturationBeta = saturation_beta;
		}

		else {
				ROS_INFO("Setting global frame to map");
				global_frame = "map";
				Conf().doImageSaturation = false;
				Conf().saturationAlpha = 1.0;
				Conf().saturationBeta = 1;
		}
}

void loadExtrinsics(ros::NodeHandle nh_, Sophus::SE3d &extrinsics,
                    bool &doLeftRightStereo) {

		std::vector<double> _R;
		std::vector<double> _T;
		Eigen::Matrix3d R;
		Eigen::Vector3d T;

		if (nh_.getParam("do_left_right_stereo", doLeftRightStereo)) {
		} else {
				ROS_INFO("Not doing left right stereo");
				doLeftRightStereo = false;
				extrinsics = Sophus::SE3d();
				return;
		}
		if (doLeftRightStereo == false) {
				ROS_INFO("Not doing left right stereo");
				extrinsics = Sophus::SE3d();
				return;
		}
		ROS_INFO("Will do left right stereo in LSD SLAM");
		doLeftRightStereo = true;
		int idx = 0;
		if (nh_.getParam("/stereo_extrinsics/R/data", _R)) {
				for (int i = 0; i < 3; i++) {
						for (int j = 0; j < 3; j++) {
								R(i, j) = _R.at(idx);
								idx++;
						}
				}
		} else {
				ROS_ERROR(
						"Failed to load stereo rotation matrix. Suppressing left right stereo");
				R = Eigen::Matrix3d::Identity();
				doLeftRightStereo = false;
		}
		if (nh_.getParam("/stereo_extrinsics/t/data", _T)) {
				T = Eigen::Vector3d(_T.data());
		} else {
				ROS_ERROR("Failed to load stereo translation matrix. Suppressing left "
				          "right stereo");
				T = Eigen::Vector3d(0, 0, 0);
				doLeftRightStereo = false;
		}
		extrinsics = Sophus::SE3d(R, T);
}

int main(int argc, char **argv) {

		// Setup loggers
		libg3logger::G3Logger<ROSLogSink> logWorker(argv[0]);
		logWorker.logBanner();
		logWorker.verbose(2);

		// init ros
		ros::init(argc, argv, "lsd_slam");
		ros::NodeHandle nh_("lsd_slam");

		// Get all neccessary ROS params
		std::string left_image_topic, left_info_topic, right_image_topic,
		            right_info_topic, disparity_map_topic, odom_topic;

		// left_image_topic = "/camera/left/camera_crop/image_crop";
		// left_info_topic = "/camera/left/downsample_camera_info";
		// right_image_topic = "/camera/right/camera_crop/image_crop";
		// right_info_topic = "/camera/right/downsample_camera_info";
		// disparity_map_topic = "/camera/disparityIdepth";
		// odom_topic = "/odometry/filtered";

		left_image_topic = "left/image_raw";
		left_info_topic = "left/camera_info";
		right_image_topic = "right/image_raw";
		right_info_topic = "right/camera_info";
		disparity_map_topic = "disparityIdepth";
		odom_topic = "odometry";

		getROSParams(nh_);

		// Get stereo extrinsics
		Sophus::SE3d extrinsics;
		bool doLeftRightStereo;

		loadExtrinsics(nh_, extrinsics, doLeftRightStereo);

		std::shared_ptr<SlamSystem> system(new SlamSystem());
		std::shared_ptr<ROSOutput3DWrapper> outputWrapper(new ROSOutput3DWrapper());

		bool run_on_start;
		if (nh_.getParam("run_on_start", run_on_start)) {
				if (run_on_start) {
						ROS_INFO("Running SLAM on node start");
				} else {
						ROS_INFO("Will not run until start service is called");
				}
		} else {
				ROS_INFO("Running SLAM on node start");
				run_on_start = true;
		}

		LOG(INFO) << "Starting input thread.";
		ROSInputThread input(system, outputWrapper, Conf().refFrame, extrinsics,
		                     run_on_start);

		ros::Subscriber leftInfoSubscriber = nh_.subscribe<sensor_msgs::CameraInfo>(
				left_info_topic, 1,
				boost::bind(&ROSInputThread::cameraInfoCallback, &input, _1, "left"));
		ros::Subscriber rightInfoSubscriber = nh_.subscribe<sensor_msgs::CameraInfo>(
				right_info_topic, 1,
				boost::bind(&ROSInputThread::cameraInfoCallback, &input, _1, "right"));
		ros::Subscriber rightImageSubscriber = nh_.subscribe<sensor_msgs::Image>(
				right_image_topic, 1,
				boost::bind(&ROSInputThread::imageCallback, &input, _1, "right"));

		ros::Subscriber leftImageSubscriber = nh_.subscribe<sensor_msgs::Image>(
				left_image_topic, 1,
				boost::bind(&ROSInputThread::imageCallback, &input, _1, "left"));

		ros::Subscriber disparitySubscriber =
				nh_.subscribe<slam_msgs::disparityIdepthMsg>(
						disparity_map_topic, 1, &ROSInputThread::disparityMapCallback,
						&input);

		ros::Subscriber odomSubscriber = nh_.subscribe<nav_msgs::Odometry>(
				odom_topic, 1, &ROSInputThread::odometryCallback, &input);

		// Sync the left image with the disparity map
		message_filters::Subscriber<sensor_msgs::Image> image_sub(
				nh_, left_image_topic, 1);
		message_filters::Subscriber<slam_msgs::disparityIdepthMsg> disparity_sub(
				nh_, disparity_map_topic, 1);
		message_filters::Synchronizer<SyncPolicy> sync(SyncPolicy(10), image_sub,
		                                               disparity_sub);

		sync.registerCallback(
				boost::bind(&ROSInputThread::imageSyncCallback, &input, _1, _2));

		// LOG(INFO) << "Conf().syncDisparityImage && Conf().doStereo: "
		//           << Conf().syncDisparityImage << " " << Conf().doStereo;
		bool disparity_image_sync_callback =
				Conf().syncDisparityImage && Conf().doStereo;

		if (disparity_image_sync_callback) {
				ROS_INFO("un-synced shutdown");
				leftImageSubscriber.shutdown();
				disparitySubscriber.shutdown();
		} else {
				ROS_INFO("synced shutdown");
				image_sub.unsubscribe();
				disparity_sub.unsubscribe();
		}

		// Dynamic Reconfigure
		dynamic_reconfigure::Server<lsd_slam_ros::lsdSlamConfig> server;
		dynamic_reconfigure::Server<lsd_slam_ros::lsdSlamConfig>::CallbackType f;

		f = boost::bind(&ROSInputThread::dynamicReconfigureCallback, &input, _1, _2);
		server.setCallback(f);

		ros::ServiceServer resetService =
				nh_.advertiseService("full_reset", &ROSInputThread::fullReset, &input);

		ros::ServiceServer startService =
				nh_.advertiseService("start", &ROSInputThread::startSlam, &input);

		ros::ServiceServer stopService =
				nh_.advertiseService("stop", &ROSInputThread::stopSlam, &input);

		boost::thread inputThread(boost::ref(input));
		input.inputReady.wait();

		// Wait for all threads to be ready.
		LOG(INFO) << "Starting all threads.";
		startAll.notify();
		// Idle loop
		ros::Rate loopRate(10);
		while (ros::ok()) {
				if (input.inputDone.getValue())
						break;

				loopRate.sleep();
		}

		LOG(INFO) << "Finalizing system.";
		system->finalize();

		return 0;
}
