#include "lsd_slam_ros/disparityMapDownsample.h"

#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/synchronizer.h>
#include <message_filters/time_synchronizer.h>

typedef message_filters::sync_policies::ApproximateTime<
				stereo_msgs::DisparityImage, sensor_msgs::Image>
        SyncPolicy;

DisparityMapDownsample::DisparityMapDownsample(int _downsample, bool _debugPC)
		: downsampleX(_downsample), downsampleY(_downsample), debugPC(_debugPC) {

		std::string disparity_channel = nh_.resolveName("disparityIdepth");
		disparity_publisher =
				nh_.advertise<slam_msgs::disparityIdepthMsg>(disparity_channel, 1);
		std::string image_channel = nh_.resolveName("synced_idepth_image");
		image_publisher = nh_.advertise<sensor_msgs::Image>(image_channel, 1);
		pcPub = nh_.advertise<PointCloudI>("downsampled_PC", 1);

		K = Eigen::Matrix3f::Identity();
}

void DisparityMapDownsample::infoCallback(
		const sensor_msgs::CameraInfoConstPtr &msg) {
		int binning_x, binning_y;
		if (msg->binning_x == 0) {
				binning_x = 1;
		}
		else{
				binning_x = msg->binning_x;
		}
		if (msg->binning_y == 0) {
				binning_y = 1;
		}
		else{
				binning_y = msg->binning_y;
		}
		K(0, 0) = msg->P[0] / binning_x;
		K(0, 2) = msg->P[2] / binning_x;
		K(1, 1) = msg->P[5] / binning_y;
		K(1, 2) = msg->P[6] / binning_y;
}

void DisparityMapDownsample::imageSyncCallback(
		const stereo_msgs::DisparityImageConstPtr &disparityMsg,
		const sensor_msgs::ImageConstPtr &imgMsg) {

		sensor_msgs::Image lsdImg = *imgMsg;
		cv::Mat callbackImage = cv_bridge::toCvShare(imgMsg, "mono8")->image;

		// Disparity callback
		img = cv_bridge::toCvCopy(disparityMsg->image, "32FC1")->image;

		f = disparityMsg->f;
		T = disparityMsg->T;
		cv::Mat disparityImg = img;
		float min_disparity = disparityMsg->min_disparity;
		float max_disparity = disparityMsg->max_disparity;

		slam_msgs::disparityIdepthMsg iDepthMsg;

		if (disparityImg.rows > 0) {
				// Convert to depth
				int count = 0;
				for (int y = 0; y < disparityImg.rows; y++) {
						for (int x = 0; x < disparityImg.cols; x++) {
								float px = disparityImg.at<float>(y, x);
								if (px < max_disparity && px > min_disparity) {
										float depth = (f * T) / px;
										// std::cout << "px: " << px << std::endl;
										// std::cout << "depth: " << depth << std::endl;
										disparityImg.at<float>(y, x) = depth;
								}
								else{
										disparityImg.at<float>(y, x) = 0;
								}
						}
				}
		}


		// Downsample
		cv::Size size((disparityImg.cols), (disparityImg.rows));

		// LOG(INFO) << "size: " << size;
		cv::Mat iDepthImg(size, CV_32F);

		cv::resize(disparityImg, iDepthImg, size, K(0, 0), K(1, 1));

		// Check for valid points
		PointCloudI::Ptr pc_debug_msg(new PointCloudI);
		if (debugPC) {
				pc_debug_msg->header.frame_id = "camera_left";
				pc_debug_msg->points.resize(iDepthImg.cols * iDepthImg.rows);
		}
		int count = 0;
		int fudge = 0.05; //So we're not doing a float compare... distance needs to be non zero from camera
		float iDepthSum = 0;
		int iDepthValCount = 0;
		int invalid = 0;
		for (int y = 0; y < iDepthImg.rows; y++) {
				for (int x = 0; x < iDepthImg.cols; x++) {
						float depth = iDepthImg.at<float>(y, x);
						if (depth < 0)
								depth *= -1;
						// std::cout << "depth: " << depth << std::endl;
						iDepthImg.at<float>(y, x) *= 100;

						if (depth > fudge && !std::isinf(depth)) {
								iDepthMsg.iDepth.push_back(1.0 / depth);
								uint8_t _true = 1;
								iDepthMsg.iDepthValid.push_back(_true);

								iDepthSum += 1.0 / depth;
								iDepthValCount++;
								if (debugPC) {
										Eigen::Vector3f X_px(x, y, 1);
										Eigen::Vector3f X_w = K.inverse() * X_px;
										pc_debug_msg->points[count].x = (X_w(0) / X_w(2)) * depth;
										pc_debug_msg->points[count].y = (X_w(1) / X_w(2)) * depth;
										pc_debug_msg->points[count].z = depth;
								}

						} else {
								invalid++;
								iDepthMsg.iDepth.push_back(-1);
								uint8_t _false = 0;
								iDepthMsg.iDepthValid.push_back(_false);
						}
						count++;
				}
		}
		// cv::imshow("iDepthImg", iDepthImg);
		// cv::waitKey(1);
		// std::cout << "Valid, invalid: " << std::endl;
		// std::cout << iDepthValCount << "," << invalid << std::endl;
		iDepthMean = iDepthSum / iDepthValCount;
		ros::Time time = ros::Time::now();
		iDepthMsg.header.stamp = time;
		iDepthMsg.iDepthMean = iDepthMean;
		disparity_publisher.publish(iDepthMsg);

		lsdImg.header.stamp = time;

		image_publisher.publish(lsdImg);

		if (pcPub) {
				pcl_conversions::toPCL(ros::Time::now(), pc_debug_msg->header.stamp);
				pcPub.publish(pc_debug_msg);
		}
}

int main(int argc, char **argv) {
		// init ros
		ros::init(argc, argv, "disparity_map");
		ros::NodeHandle nh_;

		std::string disparity_topic, info_topic;
		int downsample;
		bool debugPC;

		// if (nh_.getParam(nh_.resolveName("disparity_topic"), disparity_topic)) {
		//      ROS_INFO("Subscribing to topic : %s for dispairty map",
		//               disparity_topic.c_str());
		// } else {
		//      ROS_WARN("No left dispairty map topic found, subscribing to
		//      dispairty"); disparity_topic = nh_.resolveName("disparity");
		// }
		// if (nh_.getParam(nh_.resolveName("info_topic"), disparity_topic)) {
		//      ROS_INFO("Subscribing to topic : %s for left camera info",
		//               info_topic.c_str());
		// } else {
		//      ROS_WARN(
		//              "No left camera info topic specified, subscribing to
		//              left/camera_info");
		//      info_topic = nh_.resolveName("left/downsample_camera_info");
		// }
		if (nh_.getParam(nh_.resolveName("disparity_map_downsample/downsample"),
		                 downsample)) {
				ROS_INFO("Downsampling by : %i ", downsample);
		} else {
				ROS_INFO("Downsampling by 4");
				downsample = 1;
		}
		if (nh_.getParam(nh_.resolveName("display_downsampled_pointcloud"),
		                 debugPC)) {
				ROS_INFO("Decision to display downsampled pointcloud : %i ", debugPC);
		} else {
				ROS_INFO("Will not display downsampled pointcloud");
				debugPC = false;
		}

		DisparityMapDownsample disparityMap(downsample, debugPC);

		ros::Subscriber infoSub =
				nh_.subscribe("camera_info", 100,
				              &DisparityMapDownsample::infoCallback, &disparityMap);

		message_filters::Subscriber<stereo_msgs::DisparityImage> disparity_sub(
				nh_, "disparity", 1);
		message_filters::Subscriber<sensor_msgs::Image> image_sub(
				nh_, "left/image_rect", 1);
		message_filters::Synchronizer<SyncPolicy> sync(SyncPolicy(10), disparity_sub,
		                                               image_sub);

		sync.registerCallback(boost::bind(&DisparityMapDownsample::imageSyncCallback,
		                                  &disparityMap, _1, _2));

		ros::spin();

		return 0;
}
