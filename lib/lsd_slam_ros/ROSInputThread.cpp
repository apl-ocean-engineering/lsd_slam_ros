/**
 * This file is part of LSD-SLAM.
 *
 * Copyright 2019 Aaron Marburg <amarburg at uw dot edu >
 *
 * LSD-SLAM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LSD-SLAM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LSD-SLAM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "lsd_slam_ros/ROSInputThread.h"
#include "nav_msgs/Odometry.h"

static const std::string OPENCV_WINDOW = "Image window";

namespace lsd_slam {
// Add constructor to handle ROS output
ROSInputThread::ROSInputThread(
		std::shared_ptr<lsd_slam::SlamSystem> &sys,
		std::shared_ptr<lsd_slam::ROSOutput3DWrapper> &owrap, int _refFrame,
		bool _runOnStart)
		: system(sys), outputWrapper(owrap), inputDone(false), inputReady(),
		output(nullptr), refFrame(_refFrame), newImage(false),
		runOnStart(_runOnStart), serviceStart(false), serviceStop(false)

{
		input_odom_pub = nh_.advertise<nav_msgs::Odometry>("/lsd_slam/input_odom", 1);
		imageVector.resize(2);
		infoVector.resize(2);
		this->outputWrapper = outputWrapper;
		system->addOutputWrapper(outputWrapper);

		prev_time = ros::Time::now();

		LOG(INFO) << "ROSInputThread constructor";
}

ROSInputThread::ROSInputThread(
		std::shared_ptr<lsd_slam::SlamSystem> &sys,
		std::shared_ptr<lsd_slam::ROSOutput3DWrapper> &owrap, int _refFrame,
		Sophus::SE3d _extrinsics, bool _runOnStart)
		: system(sys), outputWrapper(owrap), inputDone(false), inputReady(),
		output(nullptr), refFrame(_refFrame), extrinsics(_extrinsics),
		newImage(false), runOnStart(_runOnStart), serviceStart(false),
		serviceStop(false)
// it_(nh_)
{
		input_odom_pub = nh_.advertise<nav_msgs::Odometry>("/lsd_slam/input_odom", 1);

		imageVector.resize(2);
		infoVector.resize(2);
		system->addOutputWrapper(outputWrapper);

		recitifcationMatrix = Eigen::Matrix3f::Identity();

		LOG(INFO) << "ROSInputThread constructor";
}

ROSInputThread::~ROSInputThread() {
		cv::destroyWindow(OPENCV_WINDOW);
}

cv::Mat ROSInputThread::parseImage(const sensor_msgs::ImageConstPtr &msg) {
		cv::Mat callbackImage;

		//TODO I'm at a loss as to by these need to be encoded in this mannor. Could be an input image issue?
		try {
				// LOG(INFO) << "msg->encoding: " << msg->encoding;
				if (msg->encoding == "mono8") {
						LOG(DEBUG) << "mono8 image type detected";
						callbackImage = cv_bridge::toCvShare(msg, "mono8")->image;
						// cvtColor(callbackImage, callbackImage, CV_BGR2GRAY);
				}
				else if (msg->encoding == "rgb8") {
						LOG(DEBUG) << "rgb8 image type detected";
						callbackImage = cv_bridge::toCvShare(msg, "rgb8")->image;
						cvtColor(callbackImage, callbackImage, CV_BGR2GRAY);
				} else if (msg->encoding == "bgr8") {
						LOG(DEBUG) << "bgr8 image type detected";
						callbackImage = cv_bridge::toCvShare(msg, "bgr8")->image;
						cvtColor(callbackImage, callbackImage, CV_BGR2GRAY);
				} else if (msg->encoding == "bgra8") {
						LOG(DEBUG) << "bgra8 image type detected";
						callbackImage = cv_bridge::toCvShare(msg, "bgra8")->image;
						cvtColor(callbackImage, callbackImage, CV_BGR2GRAY);
				} else if (msg->encoding == "bayer_grbg8") {
						LOG(DEBUG) << "bgra8 image type detected";
						callbackImage = cv_bridge::toCvShare(msg, "bayer_grbg8")->image;
						cvtColor(callbackImage, callbackImage, CV_BGR2GRAY);
				}
				else if (msg->encoding == "bayer_rggb8") {
						LOG(DEBUG) << "bayer_rggb8 image type detected";
						callbackImage = cv_bridge::toCvShare(msg, "bayer_rggb8")->image;
						cvtColor(callbackImage, callbackImage, CV_BGR2GRAY);
				}
				else {
						LOG(WARNING) << "UNKNOWN IMAGE TYPE: " << msg->encoding;
						exit(1);
				}
				if (Conf().doImageSharpen) {
						cv::Mat sharpenDst, medianDst;
						cv::Mat kernel = cv::Mat::ones(Conf().saturationKernelSize,
						                               Conf().saturationKernelSize, CV_32F) *= -1;
						kernel.at<float>((Conf().saturationKernelSize + 1) / 2,
						                 (Conf().saturationKernelSize + 1) / 2) = 9.0;
						cv::Point anchor = cv::Point(-1, -1);
						double delta = 0;
						int ddepth = -1;
						cv::filter2D(callbackImage, sharpenDst, ddepth, kernel, anchor, delta,
						             cv::BORDER_DEFAULT);

						medianBlur(sharpenDst, medianDst, Conf().saturationKernelSize);

						callbackImage = medianDst;
				}
				if (Conf().doImageSaturation) {
						cv::Mat dst;
						callbackImage.convertTo(dst, -1, Conf().saturationAlpha,
						                        Conf().saturationBeta);
						callbackImage = dst;
				}

		} catch (cv_bridge::Exception &e) {
				ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
		}

		return callbackImage;
}


void ROSInputThread::imageCallback(const sensor_msgs::ImageConstPtr &msg,
                                   std::string camera) {
		// LOG(WARNING) << "imageCallback"
		//              << " " << camera;
		newImage = true;
		cv::Mat callbackImage = parseImage(msg);


		if (camera == "left") {
				callbackImage.copyTo(imageVector.at(0));

		} else if (camera == "right") {
				callbackImage.copyTo(imageVector.at(1));
		} else {
				ROS_ERROR("Unknown camera");
				exit(1);
		}
}

// ROS image callback
void ROSInputThread::imageSyncCallback(
		const sensor_msgs::ImageConstPtr &imgL,
		const slam_msgs::disparityIdepthMsg::ConstPtr &disparity) {

		newImage = true;

		cv::Mat callbackImage(imgL->height, imgL->width, CV_8UC1);

		callbackImage = parseImage(imgL);

		callbackImage.copyTo(imageVector.at(0));


		cv::Mat debugDepthImg = cv::Mat::zeros(Conf().slamImageSize.height,
		                                       Conf().slamImageSize.width, CV_32FC1);

		disparityMapIdepthValid = disparity->iDepthValid;
		disparityMapIdepth = disparity->iDepth;
		iDepthMean = disparity->iDepthMean;
		//
		// uint8_t *iDepthValid = &disparityMapIdepthValid[0];
		// float *iDepth = &disparityMapIdepth[0];
		// int s = 0;
		// for (int y = 0; y < Conf().slamImageSize.height; y++) {
		//      for (int x = 0; x < Conf().slamImageSize.width; x++) {
		//              float id = *iDepth;
		//              bool valid = *iDepthValid;
		//              if (valid) {
		//                      debugDepthImg.at<float>(y, x) = (1.0 / id) * 100;
		//                      s++;
		//              }
		//              iDepth++;
		//              iDepthValid++;
		//      }
		// }


		if (Conf().displayInputFusedImage) {
				cv::Mat dst;
				debugDepthImg.convertTo(debugDepthImg, CV_8U);
				addWeighted(callbackImage, 0.5, debugDepthImg, 0.5, 0.0, dst);
				cv::imshow("Fused image", dst);
				cv::waitKey(1);
		}
}

bool ROSInputThread::fullReset(std_srvs::SetBool::Request &req,
                               std_srvs::SetBool::Response &res) {
		res.success = true;

		fullResetRequested = req.data;

		return true;
}

bool ROSInputThread::startSlam(std_srvs::SetBool::Request &req,
                               std_srvs::SetBool::Response &res) {
		res.success = true;

		serviceStart = req.data;

		return true;
}

bool ROSInputThread::stopSlam(std_srvs::SetBool::Request &req,
                              std_srvs::SetBool::Response &res) {
		res.success = true;

		serviceStop = req.data;

		return true;
}

void ROSInputThread::cameraInfoCallback(
		const sensor_msgs::CameraInfoConstPtr &msg, std::string camera) {
		CameraInfo = *msg;

		// USE 'P' MATRIX FOR RECTIFIED CAMERA INFO
		int binning_x, binning_y;
		if (msg->binning_x == 0) {
				binning_x = 1;
		}
		else{
				binning_x = msg->binning_x;
		}
		if (msg->binning_y == 0) {
				binning_y = 1;
		}
		else{
				binning_y = msg->binning_y;
		}
		_camera = Camera(
				CameraInfo.P[0] / binning_x, CameraInfo.P[5] / binning_x,
				CameraInfo.P[2] / binning_y, CameraInfo.P[6] / binning_y);

		if (camera == "left") {
				infoVector.at(0) = _camera;
				// recitifcationMatrix = Eigen::Matrix3f(CameraInfo.R);
				for (int i = 0; i++; i < 9) {
						recitifcationMatrix << (double)CameraInfo.R.at(i);
				}
		} else if (camera == "right") {
				infoVector.at(1) = _camera;
		} else {
				ROS_ERROR("Unknown camera");
				exit(1);
		}
		// LOG(INFO)<< "cams.at(0): " << infoVector.at(0).fx << " " << infoVector.at(0).fy;
}

void ROSInputThread::disparityMapCallback(
		const slam_msgs::disparityIdepthMsg::ConstPtr &disparity) {

		LOG(WARNING) << Conf().slamImageSize.height << " "
		             << Conf().slamImageSize.width;
		cv::Mat debugDepthImg = cv::Mat::zeros(Conf().slamImageSize.height,
		                                       Conf().slamImageSize.width, CV_32FC1);

		disparityMapIdepthValid = disparity->iDepthValid;
		disparityMapIdepth = disparity->iDepth;
		iDepthMean = disparity->iDepthMean;

		uint8_t *iDepthValid = &disparityMapIdepthValid[0];
		float *iDepth = &disparityMapIdepth[0];

		for (int y = 0; y < Conf().slamImageSize.height; y++) {
				for (int x = 0; x < Conf().slamImageSize.width; x++) {
						float id = *iDepth;
						bool valid = *iDepthValid;
						if (valid) {
								debugDepthImg.at<float>(y, x) = (1.0 / id) * 100;
						}
						iDepth++;
						iDepthValid++;
				}
		}
		cv::Mat dst;
		debugDepthImg.convertTo(debugDepthImg, CV_8U);

		addWeighted(callbackImage, 0.5, debugDepthImg, 0.5, 0.0, dst);
		if (Conf().displayInputFusedImage) {
				cv::imshow("Fused image", dst);
				cv::waitKey(1);
		}
}

void ROSInputThread::odometryCallback(const nav_msgs::Odometry::ConstPtr &msg) {
		// LOG(WARNING) << "odometryCallback";
		ros::Time current_time = msg->header.stamp;
		ros::Duration duration = current_time - prev_time;
		if (first_odom) {
				first_odom = false;
				prev_time = current_time;
				return;
		}

		if (abs(msg->twist.twist.linear.x * duration.toSec()) > 5.0 ||
		    abs(msg->twist.twist.linear.y* duration.toSec()) > 5.0 ||
		    abs(msg->twist.twist.linear.z * duration.toSec()) > 5.0) {
				prev_time = current_time;
				return;
		}
		// odomLinearTwist(0) = msg->twist.twist.linear.x * duration.toSec();
		// odomLinearTwist(1) = msg->twist.twist.linear.y * duration.toSec();
		// odomLinearTwist(2) = msg->twist.twist.linear.z * duration.toSec();
		//
		//
		//
		// LOG(INFO) << "odomLinearTwist: " << odomLinearTwist;
		//
		// odomAngularTwist(0) = msg->twist.twist.angular.x * duration.toSec();
		// odomAngularTwist(1) = msg->twist.twist.angular.y * duration.toSec();
		// odomAngularTwist(2) = msg->twist.twist.angular.z * duration.toSec();

		odomLinearTwist(0) = msg->twist.twist.linear.x * duration.toSec();
		odomLinearTwist(1) = msg->twist.twist.linear.y * duration.toSec();
		odomLinearTwist(2) = msg->twist.twist.linear.z * duration.toSec();

		// LOG(INFO) << "odomLinearTwist: " << odomLinearTwist;

		odomAngularTwist(0) = msg->twist.twist.angular.x * duration.toSec();
		odomAngularTwist(1) = msg->twist.twist.angular.y * duration.toSec();
		odomAngularTwist(2) = msg->twist.twist.angular.z * duration.toSec();

		input_odom_pub.publish(*msg);

		prev_time = current_time;
		odomSet = true;
}

void ROSInputThread::cfgOutput(std::string name, double val1, double val2) {
		LOG(WARNING) << "Updating: " << name << " from " << val1 << " to " << val2;
}
void ROSInputThread::cfgOutput(std::string name, int val1, int val2) {
		LOG(WARNING) << "Updating: " << name << " from " << val1 << " to " << val2;
}
void ROSInputThread::cfgOutput(std::string name, bool val1, bool val2) {
		LOG(WARNING) << "Updating: " << name << " from " << val1 << " to " << val2;
}

void ROSInputThread::dynamicReconfigureCallback(
		lsd_slam_ros::lsdSlamConfig &config, uint32_t level) {
		if (Conf().minAbsGradCreate != config.min_abs_create) {
				cfgOutput("min_abs_create", (double)Conf().minAbsGradCreate,
				          config.min_abs_create);
				Conf().minAbsGradCreate = config.min_abs_create;
		}

		if (Conf().minAbsGradDecrease != config.min_abs_decrease) {
				cfgOutput("min_abs_decrease", (double)Conf().minAbsGradDecrease,
				          config.min_abs_decrease);
				Conf().minAbsGradDecrease = config.min_abs_decrease;
		}
		//
		// if (Conf().publishSetPose != config.publish_set_pose) {
		//      cfgOutput("publish_set_pose", Conf().publishSetPose,
		//                config.publish_set_pose);
		//      Conf().publishSetPose = config.publish_set_pose;
		// }
		//
		// if (Conf().minVirtualBaselineLength !=
		//     config.groups.lsd_slam_processing.min_virtual_baseline_length) {
		//      cfgOutput("min_virtual_baseline_length",
		//                (double)Conf().minVirtualBaselineLength,
		//                config.groups.lsd_slam_processing.min_virtual_baseline_length);
		//      Conf().minVirtualBaselineLength =
		//              config.groups.lsd_slam_processing.min_virtual_baseline_length;
		// }
		//
		// if (Conf().displayDepthMap !=
		//     config.groups.lsd_slam_debugging.display_depth_map) {
		//      cfgOutput("display_depth_map", Conf().displayDepthMap,
		//                config.groups.lsd_slam_debugging.display_depth_map);
		//      Conf().displayDepthMap = config.groups.lsd_slam_debugging.display_depth_map;
		// }
		//
		// if (Conf().displayGradientMap !=
		//     config.groups.lsd_slam_debugging.display_gradient_map) {
		//      cfgOutput("display_gradient_map", Conf().displayGradientMap,
		//                config.groups.lsd_slam_debugging.display_gradient_map);
		//      Conf().displayGradientMap =
		//              config.groups.lsd_slam_debugging.display_gradient_map;
		// }
		// if (Conf().displayInputImage !=
		//     config.groups.lsd_slam_debugging.display_input_image) {
		//      cfgOutput("display_input_image", Conf().displayInputImage,
		//                config.groups.lsd_slam_debugging.display_input_image);
		//      Conf().displayInputImage =
		//              config.groups.lsd_slam_debugging.display_input_image;
		// }
		// if (Conf().displayInputFusedImage !=
		//     config.groups.lsd_slam_debugging.display_input_fused_image) {
		//      cfgOutput("display_input_fused_image", Conf().displayInputFusedImage,
		//                config.groups.lsd_slam_debugging.display_input_fused_image);
		//      Conf().displayInputFusedImage =
		//              config.groups.lsd_slam_debugging.display_input_fused_image;
		// }
		// if (Conf().print.constraintSearchInfo !=
		//     config.groups.lsd_slam_debugging.print_constraint_info) {
		//      cfgOutput("print_constraint_info", Conf().print.constraintSearchInfo,
		//                config.groups.lsd_slam_debugging.print_constraint_info);
		//      Conf().print.constraintSearchInfo =
		//              config.groups.lsd_slam_debugging.print_constraint_info;
		// }
		// if (Conf().print.threadingInfo !=
		//     config.groups.lsd_slam_debugging.print_threading_info) {
		//      cfgOutput("print_threading_info", Conf().print.threadingInfo,
		//                config.groups.lsd_slam_debugging.print_threading_info);
		//      Conf().print.threadingInfo =
		//              config.groups.lsd_slam_debugging.print_threading_info;
		// }
		// if (Conf().doImageSaturation !=
		//     config.groups.slam_image_processing.image_saturation) {
		//      cfgOutput("image_saturation", Conf().doImageSaturation,
		//                config.groups.slam_image_processing.image_saturation);
		//      Conf().doImageSaturation =
		//              config.groups.slam_image_processing.image_saturation;
		// }
		// if (Conf().saturationAlpha !=
		//     config.groups.slam_image_processing.saturation_alpha) {
		//      cfgOutput("saturation_alpha", (double)Conf().saturationAlpha,
		//                config.groups.slam_image_processing.saturation_alpha);
		//      Conf().saturationAlpha =
		//              config.groups.slam_image_processing.saturation_alpha;
		// }
		// if (Conf().saturationBeta !=
		//     config.groups.slam_image_processing.saturation_beta) {
		//      cfgOutput("saturation_beta", Conf().saturationBeta,
		//                config.groups.slam_image_processing.saturation_beta);
		//      Conf().saturationBeta = config.groups.slam_image_processing.saturation_beta;
		// }
}

// Where input thread enters
void ROSInputThread::operator()() {
		// get HZ
		LOG(INFO) << " !!! Starting ROSInputThread !!!!";
		// cv::namedWindow(OPENCV_WINDOW);

		float fps = 30;
		// Setup
		long int dt_us = (fps > 0) ? (1e6 / fps) : 0;
		long int dt_fudge = 0;
		// inputReady.notify();
		// startAll.wait();

		cv::Mat image = cv::Mat(Conf().slamImageSize.cvSize(), CV_8U);

		int runningIdx = 0;
		float fakeTimeStamp = 0;
		ros::Rate loop_rate(fps);
		int preMotionCout = 0;
		while (ros::ok()) {
				if (runningIdx == 0) {
						// Wait for everything to catch up...
						// ros::Duration(1.0).sleep();
				}

				if (fullResetRequested) {
						LOG(WARNING) << "FULL RESET!";
						system.reset(system->fullReset());

						fullResetRequested = false;
						runningIdx = 0;
				}
				std::chrono::time_point<std::chrono::steady_clock> start(
						std::chrono::steady_clock::now());
				preMotionCout++;

				bool do_lsd_slam;
				if (Conf().doStereo) {
						do_lsd_slam =
								imageVector.at(refFrame).size() == Conf().slamImageSize.cvSize() &&
								disparityMapIdepth.size() > 0 && newImage && preMotionCout > 10;
				} else {
						do_lsd_slam =
								imageVector.at(refFrame).size() == Conf().slamImageSize.cvSize() &&
								newImage && preMotionCout > 10;
				}

				// LOG(INFO) << imageVector.at(refFrame).size() << " " << Conf().slamImageSize.cvSize();
				// LOG(INFO) << "K: " << infoVector.at(refFrame).K << "\n";

				bool SLAMStartready;

				if (runOnStart) {
						SLAMStartready = true;
				} else {
						SLAMStartready = serviceStart;
				}

				if (serviceStop && runningIdx % 10 == 0) {
						LOG(WARNING) << "Stop running SLAM indicated";
				}

				if (!SLAMStartready && runningIdx % 10 == 0) {
						LOG(WARNING)
						        << "No indication to begin SLAM. Start using service call start";
				}

				if (do_lsd_slam && SLAMStartready && !serviceStop) {
						//
						LOG(INFO) << "!!! Processing new image !!!";

						// This will block if system->conf().runRealTime is false
						LOG(DEBUG) << "!!! Passing new image to slamSystem";
						newImage = false;

						if (Conf().displayInputImage) {
								cv::Mat displayImg;
								cv::imshow(OPENCV_WINDOW, imageVector.at(0));
								cv::waitKey(1);
						}
						// LOG(INFO) << "infoVector.at(0)\n" << infoVector.at(0).K;
						//
						CHECK(imageVector.at(0).type() == CV_8UC1 &&
						      imageVector.at(1).type() == CV_8UC1);

						ImageSet::SharedPtr imageSet;
						//
						if (Conf().doStereo) {
								imageSet = std::make_shared<ImageSet>(runningIdx, imageVector,
								                                      infoVector, refFrame);
								if (Conf().doLeftRightStereo) {
										imageSet->setFrameToRef(1, extrinsics);
								}

								if (odomSet)
										imageSet->setOdomMotionEstimate(odomLinearTwist, odomAngularTwist);

								int size = disparityMapIdepthValid.size();
								uint8_t *iDepthValid = &disparityMapIdepthValid[0];
								float *iDepth = &disparityMapIdepth[0];

								imageSet->setDisparityMap(iDepth, iDepthValid, iDepthMean, size);

								system->nextImageSet(imageSet);
						} else {
								system->nextImage(runningIdx, imageVector.at(0), infoVector.at(0));
						}

						fakeTimeStamp += (fps > 0) ? (1.0 / fps) : 0.03;
				}
				runningIdx++;

				if (Conf().runRealTime && dt_us > 0)
						std::this_thread::sleep_until(
								start + std::chrono::microseconds(dt_us + dt_fudge));
				loop_rate.sleep();
				ros::spinOnce();
		}
}

} // namespace lsd_slam
