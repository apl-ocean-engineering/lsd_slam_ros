/**
 * This file is part of LSD-SLAM.
 *
 * Copyright 2013 Jakob Engel <engelj at in dot tum dot de> (Technical
 * University of Munich) For more information see
 * <http://vision.in.tum.de/lsdslam>
 *
 * LSD-SLAM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LSD-SLAM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LSD-SLAM. If not, see <http://www.gnu.org/licenses/>.
 */

#include "lsd_slam_ros/ROSOutput3DWrapper.h"

#include "util/SophusUtil.h"
#include "util/settings.h"

#include <ros/ros.h>

// #include "geometry_msgs/IMU.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/TwistWithCovarianceStamped.h"
#include "nav_msgs/Odometry.h"
#include "sensor_msgs/PointCloud2.h"
#include "std_msgs/Float32MultiArray.h"

#include "DataStructures/Frame.h"
#include "GlobalMapping/KeyFrameGraph.h"
#include "GlobalMapping/g2oTypeSim3Sophus.h"
#include "sophus/sim3.hpp"

#include <tf/transform_datatypes.h>


namespace lsd_slam {

ROSOutput3DWrapper::ROSOutput3DWrapper() {
		frame_channel = nh_.resolveName("lsd_slam/frames");
		frame_publisher = nh_.advertise<slam_msgs::frameMsg>(frame_channel, 1);

		graph_channel = nh_.resolveName("lsd_slam/graph");
		graph_publisher =
				nh_.advertise<slam_msgs::keyframeGraphMsg>(graph_channel, 1);

		frame_pose_data_channel = nh_.resolveName("lsd_slam/framePoses");
		frame_pose_data_publisher =
				nh_.advertise<slam_msgs::framePoseData>(frame_pose_data_channel, 1);

		debugInfo_channel = nh_.resolveName("lsd_slam/debug");
		debugInfo_publisher =
				nh_.advertise<std_msgs::Float32MultiArray>(debugInfo_channel, 1);

		pose_channel = nh_.resolveName("lsd_slam/pose");
		pose_publisher = nh_.advertise<geometry_msgs::PoseStamped>(pose_channel, 1);

		pose_with_covariance_channel = nh_.resolveName("set_pose");
		pose_with_covariance_publisher =
				nh_.advertise<geometry_msgs::PoseWithCovarianceStamped>(
						pose_with_covariance_channel, 1);

		twist_channel = nh_.resolveName("lsd_slam/twist");
		twist_publisher =
				nh_.advertise<geometry_msgs::TwistWithCovarianceStamped>(twist_channel, 1);

		odom_channel = nh_.resolveName("lsd_slam/odom");
		odom_publisher = nh_.advertise<nav_msgs::Odometry>(odom_channel, 1);

		pointcloud_channel = nh_.resolveName("lsd_slam/pointcloud");
		pointcloud_publisher = nh_.advertise<PointCloud>(pointcloud_channel, 1);

		image_channel = nh_.resolveName("lsd_slam/debug_image");
		image_pub = nh_.advertise<sensor_msgs::Image>(image_channel, 1);

		publishLvl = 0;
}

ROSOutput3DWrapper::~ROSOutput3DWrapper() {
}

void ROSOutput3DWrapper::keyFrameCallback(
		const slam_msgs::keyframeMsg::ConstPtr &kfMsg) {
		// Construct pointcloud after each message is published
		// sensor_msgs::PointCloud2 pointCloudMsg = kfMsg->pointcloud;
		PointCloud::Ptr pointcloudMsg(new PointCloud);
		pcl::fromROSMsg(kfMsg->pointcloud, *pointcloudMsg);

		// get world transformation
		Eigen::Matrix4f G = Eigen::Matrix4f::Zero(4, 4);
		Eigen::Matrix3f R;
		Eigen::Vector3f T;
		T << kfMsg->camToWorld[0], kfMsg->camToWorld[1], kfMsg->camToWorld[2];
		Eigen::Quaterniond q;
		q.x() = kfMsg->camToWorld[3];
		q.y() = kfMsg->camToWorld[4];
		q.z() = kfMsg->camToWorld[5];
		q.w() = kfMsg->camToWorld[6];

		if (q.w() < 0) {
				q.x() *= -1;
				q.y() *= -1;
				q.z() *= -1;
				q.w() *= -1;
		}
		R = q.toRotationMatrix().cast<float>();

		G.block<3, 3>(0, 0) = R;
		G.block<3, 1>(0, 3) = T;
		G(3, 3) = 1.0;

		PointCloud transformedPointcloud;
		pcl::transformPointCloud(*pointcloudMsg, transformedPointcloud, G);

		// Add new pointcloud to old pointcloud

		/*Filtering still slow
		   fullPointCloud += transformedPointcloud;
		   PointCloud::Ptr pointcloudFiltered(new PointCloud);
		   pcl::VoxelGrid<PointT> vox;
		   vox.setInputCloud (pointcloudMsg);
		   vox.setLeafSize (0.25f, 0.25f, 0.25f);
		   vox.filter (*pointcloudFiltered);

		   fullPointCloud = *pointcloudFiltered;
		   fullPointCloud.header.frame_id = "map";
		   pointcloud_publisher.publish(fullPointCloud);
		 */
		// transformedPointcloud.header.frame_id = "map";
		// pointcloud_publisher.publish(transformedPointcloud);
}

void ROSOutput3DWrapper::publishKeyframe(const Frame::SharedPtr &kf) {
}

void ROSOutput3DWrapper::publishFrame(const Frame::SharedPtr &frame,
                                      const Eigen::MatrixXf G,
                                      const DepthMap::SharedPtr &depthMap) {

		// Publish keyframe and pointcloud from keyframe perspective
		Eigen::Matrix3f R = G.block<3, 3>(0, 0);
		Eigen::Vector3f T = G.block<3, 1>(0, 3);
		slam_msgs::frameMsg fMsg;
		boost::shared_lock<boost::shared_mutex> lock = frame->getActiveLock();

		LOG(INFO) << "Current frame being published: " << frame->id();

		fMsg.id = frame->id();
		fMsg.time = frame->timestamp();
		fMsg.isKeyframe = frame->isTrackingParent(frame->id());

		unsigned int w = frame->width(publishLvl);
		unsigned int h = frame->height(publishLvl);

		// memcpy(fMsg.frameToKF.data(),
		//        (frame->getCamToWorld() * frame->trackingParent()->getCamToWorld())
		//            .cast<float>()
		//            .data(),
		//        sizeof(float) * 7);

		memcpy(fMsg.kf.camToWorld.data(), frame->getCamToWorld().cast<float>().data(),
		       sizeof(float) * 7);
		float fx = frame->fx(publishLvl);
		float fy = frame->fy(publishLvl);
		float cx = frame->cx(publishLvl);
		float cy = frame->cy(publishLvl);

		fMsg.kf.fx = frame->fx(publishLvl);
		fMsg.kf.fy = frame->fy(publishLvl);
		fMsg.kf.cx = frame->cx(publishLvl);
		fMsg.kf.cy = frame->cy(publishLvl);
		fMsg.kf.width = w;
		fMsg.kf.height = h;

		// Create PointCloud
		PointCloud::Ptr pointcloudMsg(new PointCloud);
		sensor_msgs::PointCloud2 pointcloudROSMsg;
		Eigen::Matrix3f Kinv = frame->Kinv(publishLvl);

		pointcloudMsg->points.resize(w * h);
		fMsg.kf.idepthVar.resize(w * h);

		const float *idepth = frame->idepth(publishLvl);

		const float *idepthVar = frame->idepthVar(publishLvl);
		const float *color = frame->image(publishLvl);
		unsigned int idx = 0;

		float fxi = 1.0 / fx;
		float fyi = 1.0 / fy;
		float cxi = -cx / fx;
		float cyi = -cy / fy;
		// int s = 0;
		cv::Mat debugImg = cv::Mat::zeros(h, w, CV_8UC3);
		for (int y = 1; y < h - 1; y++) {
				for (int x = 1; x < w - 1; x++) {
						idx = x + y * w;
						float depth = 1 / idepth[idx];
						float var = idepthVar[idx];

						DepthMapPixelHypothesis *DPH = depthMap->hypothesisAt(x, y);
						fMsg.kf.idepthVar[idx] = var;
						if (depth > 0) {
								cv::Vec3b c = DPH->getVisualizationColor(frame->id());
								debugImg.at<cv::Vec3b>(y, x) = c;
								float _x = (x * fxi + cxi) * depth;
								float _y = (y * fyi + cyi) * depth;
								Eigen::Vector3f X(_x, _y, depth);
								Eigen::Vector3f Xw = R * X + T;
								pointcloudMsg->points[idx].x = Xw(0);
								pointcloudMsg->points[idx].y = Xw(1);
								pointcloudMsg->points[idx].z = Xw(2);
								pointcloudMsg->points[idx].r = color[idx];
								pointcloudMsg->points[idx].g = color[idx];
								pointcloudMsg->points[idx].b = color[idx];
						}
				}
		}
		// // cv::imshow("debugImg", debugImg);
		// // cv::waitKey(1);
		// LOG(DEBUG) << "Pointcloud Size: " << s;
		//
		cv_bridge::CvImage out_img;
		out_img.header.frame_id = Conf().lsdFrame;
		out_img.header.stamp = ros::Time::now();
		out_img.encoding = "bgr8";
		out_img.image = debugImg;
		//
		image_pub.publish(out_img.toImageMsg());
		// // Filtering
		// /*
		//
		// pcl::StatisticalOutlierRemoval<PointT> sor;
		// sor.setInputCloud(pointcloudMsg);
		// sor.setMeanK(50);
		// sor.setStddevMulThresh(1.0);
		// sor.filter(*cloud_filtered);
		//
		// PointCloud::Ptr cloud_filtered(new PointCloud);
		// pcl::RadiusOutlierRemoval<PointT> rorfilter(
		//     true); // Initializing with true will allow us to extract the removed
		//            // indices
		//
		// rorfilter.setInputCloud(pointcloudMsg);
		// rorfilter.setRadiusSearch(0.1);
		// rorfilter.setMinNeighborsInRadius(5);
		// rorfilter.setNegative(true);
		// rorfilter.filter(*cloud_filtered);
		// */
		std::string rectifiedFrame = Conf().lsdFrame + "/un_rectified";
		if (Conf().useRectificationFrame) {
				pointcloudMsg->header.frame_id = rectifiedFrame;
		} else {
				pointcloudMsg->header.frame_id = Conf().lsdFrame;
		}

		pcl_conversions::toPCL(ros::Time::now(), pointcloudMsg->header.stamp);
		pointcloud_publisher.publish(pointcloudMsg);

		pcl::transformPointCloud(*pointcloudMsg, *pointcloudMsg, G);

		// convert to ROS message type
		pcl::toROSMsg(*pointcloudMsg, pointcloudROSMsg);
		fMsg.kf.pointcloud = pointcloudROSMsg;

		// Publish pointcloud here for now...
		pointcloudROSMsg.header.frame_id = Conf().lsdFrame;
		// pointcloud_publisher.publish(pointcloudROSMsg);

		fMsg.header.stamp = ros::Time::now();
		fMsg.header.frame_id = Conf().lsdFrame;

		// Publish
		frame_publisher.publish(fMsg);

		// tf::Transform transform;
		// Eigen::Matrix3f rectificationMatrix = frame->getRectificationMatrix();
		// Eigen::Quaterniond quaternion(R.cast<double>());
		// tf::Quaternion q;
		// tf::quaternionEigenToTF(quaternion, q);
		// transform.setRotation(q);
		//
		// TB.sendTransform(tf::StampedTransform(transform, ros::Time::now(),
		//                                       Conf().lsdFrame,
		//                                       Conf().lsdFrame + "/un_rectified"));
} // namespace lsd_slam

void ROSOutput3DWrapper::publishTrackedFrame(const Frame::SharedPtr &kf) {
}

void ROSOutput3DWrapper::publishTrackedFrame(
		const Frame::SharedPtr &frame, const Frame::SharedPtr &kf,
		const Eigen::MatrixXf G, const DepthMap::SharedPtr &depthMap,
		SE3 &frameToParentEstimate) {
}

void ROSOutput3DWrapper::publishKeyframeGraph(
		const std::shared_ptr<KeyFrameGraph> &graph) {
		slam_msgs::keyframeGraphMsg gMsg;
		slam_msgs::framePoseData fpMsg;

		graph->edgesListsMutex.lock();
		gMsg.numConstraints = graph->edgesAll.size();
		gMsg.constraintsData.resize(gMsg.numConstraints * sizeof(GraphConstraint));
		GraphConstraint *constraintData =
				(GraphConstraint *)gMsg.constraintsData.data();
		for (unsigned int i = 0; i < graph->edgesAll.size(); i++) {
				constraintData[i].from = graph->edgesAll[i]->firstFrame->id();
				constraintData[i].to = graph->edgesAll[i]->secondFrame->id();
				Sophus::Vector7d err = graph->edgesAll[i]->edge->error();
				constraintData[i].err = sqrt(err.dot(err));
		}
		graph->edgesListsMutex.unlock();

		graph->keyframesAllMutex.lock_shared();
		gMsg.numFrames = graph->keyframesAll.size();
		gMsg.frameData.resize(gMsg.numFrames * sizeof(GraphFramePose));
		GraphFramePose *framePoseData = (GraphFramePose *)gMsg.frameData.data();

		std_msgs::Header header;
		header.frame_id = Conf().lsdFrame;
		header.stamp = ros::Time::now();

		fpMsg.header = header;

		fpMsg.numFrames = graph->keyframesAll.size();
		fpMsg.id.resize(graph->keyframesAll.size());
		fpMsg.frameData.resize(graph->keyframesAll.size() * 7);

		for (unsigned int i = 0; i < graph->keyframesAll.size(); i++) {

				framePoseData[i].id = graph->keyframesAll[i]->id();
				fpMsg.id[i] = graph->keyframesAll[i]->id();
				memcpy(framePoseData[i].camToWorld,
				       graph->keyframesAll[i]->getCamToWorld().cast<float>().data(),
				       sizeof(float) * 7);
				for (int j = 0; j < 7; j++) {
						fpMsg.frameData[i * 7 + j] =
								graph->keyframesAll[i]->getCamToWorld().cast<float>().data()[j];
				}
		}
		graph->keyframesAllMutex.unlock_shared();

		graph_publisher.publish(gMsg);

		frame_pose_data_publisher.publish(fpMsg);
}

void ROSOutput3DWrapper::publishStateEstimation(
		const Sophus::Sim3f &pose,
		const Sophus::SE3f &motion,
		double timeElapsed) {
		geometry_msgs::TwistWithCovarianceStamped twist;
		nav_msgs::Odometry odom;

		Eigen::Quaternionf quat = motion.unit_quaternion();
		Eigen::Vector3f translation = motion.translation();

		tf::Quaternion q(quat.x(), quat.y(), quat.z(), quat.w());
		tf::Matrix3x3 m(q);
		double roll, pitch, yaw;
		m.getRPY(roll, pitch, yaw);
		//
		twist.header.frame_id = "base_link";
		twist.header.stamp = ros::Time::now();
		//
		twist.twist.twist.linear.x = translation(0)/timeElapsed;
		twist.twist.twist.linear.y = translation(1)/timeElapsed;
		twist.twist.twist.linear.z = translation(2)/timeElapsed;
		//
		twist.twist.twist.angular.x = roll/timeElapsed;
		twist.twist.twist.angular.y = pitch/timeElapsed;
		twist.twist.twist.angular.z = yaw/timeElapsed;
		// twist.twist.twist.linear.x = translation(0);
		// twist.twist.twist.linear.y = translation(1);
		// twist.twist.twist.linear.z = translation(2);
		// //
		// twist.twist.twist.angular.x = roll;
		// twist.twist.twist.angular.y = pitch;
		// twist.twist.twist.angular.z = yaw;
		//
		twist.twist.covariance[0] = 1e-7;
		twist.twist.covariance[7] = 1e-7;
		twist.twist.covariance[14] = 1e-7;
		twist.twist.covariance[21] = 1e-7;
		twist.twist.covariance[28] = 1e-7;
		twist.twist.covariance[35] = 1e-7;
		//
		Eigen::Matrix3f R = pose.rotationMatrix();
		Eigen::Quaterniond quaternion(R.cast<double>());
		Eigen::Vector3f T = pose.translation();

		// LOG(INFO) << "Output T\n" << T;

		geometry_msgs::PoseWithCovarianceStamped pwc;
		geometry_msgs::Pose rosPose;


		rosPose.position.x = T(0);
		rosPose.position.y = T(1);
		rosPose.position.z = T(2);
		rosPose.orientation.x = quaternion.x();
		rosPose.orientation.y = quaternion.y();
		rosPose.orientation.z = quaternion.z();
		rosPose.orientation.w = quaternion.w();

		if (rosPose.orientation.w < 0) {
				rosPose.orientation.x *= -1;
				rosPose.orientation.y *= -1;
				rosPose.orientation.z *= -1;
				rosPose.orientation.w *= -1;
		}
		pwc.pose.pose = rosPose;

		pwc.header.stamp = ros::Time::now();
		pwc.header.frame_id = "odom";

		pwc.pose.covariance[0] = 1e-9;
		pwc.pose.covariance[7] = 1e-9;
		pwc.pose.covariance[14] = 1e-9;
		pwc.pose.covariance[21] = 1e-9;
		pwc.pose.covariance[28] = 1e-9;
		pwc.pose.covariance[35] = 1e-9;
		//

		odom.header = twist.header;
		odom.child_frame_id = "odom";
		odom.twist = twist.twist;
		odom.pose = pwc.pose;

		twist_publisher.publish(twist);
		odom_publisher.publish(odom);
}

void ROSOutput3DWrapper::publishOptimizedPoseEstimate(
		const Sophus::Sim3f &pose) {
		Eigen::Matrix3f R = pose.rotationMatrix();
		Eigen::Quaterniond quaternion(R.cast<double>());
		Eigen::Vector3f T = pose.translation();

		geometry_msgs::Pose rosPose;
		geometry_msgs::PoseWithCovarianceStamped pwc;

		rosPose.position.x = T(0);
		rosPose.position.y = T(1);
		rosPose.position.z = T(2);
		rosPose.orientation.x = quaternion.x();
		rosPose.orientation.y = quaternion.y();
		rosPose.orientation.z = quaternion.z();
		rosPose.orientation.w = quaternion.w();

		if (rosPose.orientation.w < 0) {
				rosPose.orientation.x *= -1;
				rosPose.orientation.y *= -1;
				rosPose.orientation.z *= -1;
				rosPose.orientation.w *= -1;
		}
		pwc.pose.pose = rosPose;

		pwc.header.stamp = ros::Time::now();
		pwc.header.frame_id = "odom";

		pwc.pose.covariance[0] = 1e-9;
		pwc.pose.covariance[7] = 1e-9;
		pwc.pose.covariance[14] = 1e-9;
		pwc.pose.covariance[21] = 1e-9;
		pwc.pose.covariance[28] = 1e-9;
		pwc.pose.covariance[35] = 1e-9;

		if (Conf().publishSetPose)
				pose_with_covariance_publisher.publish(pwc);
}

void ROSOutput3DWrapper::publishPointCloud(const Frame::SharedPtr &kf) {

		/*
		   kf->getActiveLock();

		   int w = kf->width(publishLvl);
		   int h = kf->height(publishLvl);

		   //get world transformation
		   SE3 camToWorld = se3FromSim3(kf->getCamToWorld());
		   Eigen::Matrix4f G = Eigen::Matrix4f::Zero(4,4);
		   Eigen::Matrix3f R;
		   Eigen::Vector3f T;
		   Eigen::Quaterniond q;
		   q.x() = camToWorld.so3().unit_quaternion().x();
		   q.y() = camToWorld.so3().unit_quaternion().y();
		   q.z() = camToWorld.so3().unit_quaternion().z();
		   q.w() = camToWorld.so3().unit_quaternion().w();
		   if (q.w() < 0)
		   {
		        q.x() *= -1;
		        q.y() *= -1;
		        q.z() *= -1;
		        q.w() *= -1;
		   }
		   R = q.toRotationMatrix().cast<float>();
		   T(0) = camToWorld.translation()[0];
		   T(1) = camToWorld.translation()[1];
		   T(2) = camToWorld.translation()[2];
		   G.block<3,3>(0,0) = R;
		   G.block<3,1>(0,3) = T;
		   G(3,3) = 1.0;
		   Eigen::Matrix3f Kinv = kf->Kinv(publishLvl);

		   //update PointCloud
		   PointCloud::Ptr pointcloudMsg (new PointCloud);
		   pointcloudMsg->points.resize(w*h);
		   const float* idepth = kf->idepth(publishLvl);
		   const float* idepthVar = kf->idepthVar(publishLvl);
		   const float* color = kf->image(publishLvl);

		   int idx = 0;

		   for (int y=0;y<h;y++)
		   {
		        for (int x=0;x<w;x++)
		        {
		                idx += 1;
		                float z = idepth[x*y];
		                if (z > 0 && idx < pointcloudMsg->points.size())
		                {
		                        //Get accurate scale, transform frame
		                        Eigen::Vector3f xImg;
		                        xImg << x,y,1;
		                        Eigen::Vector3f xWorldRelative = Kinv*xImg;
		                        Eigen::Vector4f xWorld;
		                        xWorld << xWorldRelative(0)/xWorldRelative(2),
		                                                                xWorldRelative(1)/xWorldRelative(2),
		   z, 1.0; Eigen::Vector4f xWorldTrans = G*xWorld;
		                        //Add to pointcloud
		                        pointcloudMsg->points[idx].x =
		   xWorldTrans(0)/xWorldTrans(3); pointcloudMsg->points[idx].y =
		   xWorldTrans(1)/xWorldTrans(3); pointcloudMsg->points[idx].z =
		   xWorldTrans(2)/xWorldTrans(3);//xWorldTrans(2);
		                        //TODO forgot we're grayscale...
		                        pointcloudMsg->points[idx].intensity = color[x*y];
		                        //pointcloudMsg->points[idx].g = color[x*y];
		                        //pointcloudMsg->points[idx].b = color[x*y];

		                }

		        }
		   }
		   pointcloudMsg->header.frame_id = "map";
		   pointcloud_publisher.publish(pointcloudMsg);
		   /*
		   PointCloud::Ptr pointcloudFiltered(new PointCloud);
		   pcl::VoxelGrid<PointT> vox;
		   vox.setInputCloud (pointcloudMsg);
		   vox.setLeafSize (0.01f, 0.01f, 0.01f);
		   vox.filter (*pointcloudFiltered);
		   pointcloudMsg->header.frame_id = "map";
		   pointcloud_publisher.publish(pointcloudMsg);


		   /*
		   graph->edgesListsMutex.lock();

		   //Loop through all keyframes
		   for (unsigned int i=0; i<graph->edgesAll.size();i++)
		   {

		        Frame::SharedPtr kf = graph->keyframesAll.at(i);



		        std::cout << G << std::endl;





		        //Increase the pointcloud by the new keyframe size
		        //pointcloudMsg->points.resize(w*h + pointcloudMsg->points.size());


		   }



		   graph->edgesListsMutex.unlock();
		 */
}

void ROSOutput3DWrapper::publishTrajectory(
		std::vector<Eigen::Matrix<float, 3, 1> > trajectory,
		std::string identifier) {
		// unimplemented ... do i need it?
}

void ROSOutput3DWrapper::publishTrajectoryIncrement(
		Eigen::Matrix<float, 3, 1> pt, std::string identifier) {
		// unimplemented ... do i need it?
}

void ROSOutput3DWrapper::updateDepthImage(unsigned char *data) {
		// unimplemented ... do i need it?
}

void ROSOutput3DWrapper::publishPose(const Sophus::Sim3f &pose) {

		Eigen::Matrix3f R = pose.rotationMatrix();
		Eigen::Quaterniond quaternion(R.cast<double>());
		Eigen::Vector3f T = pose.translation();

		geometry_msgs::PoseStamped pMsg;
		geometry_msgs::Pose rosPose;
		geometry_msgs::PoseWithCovarianceStamped pwc;

		rosPose.position.x = T(0);
		rosPose.position.y = T(1);
		rosPose.position.z = T(2);
		rosPose.orientation.x = quaternion.x();
		rosPose.orientation.y = quaternion.y();
		rosPose.orientation.z = quaternion.z();
		rosPose.orientation.w = quaternion.w();

		if (rosPose.orientation.w < 0) {
				rosPose.orientation.x *= -1;
				rosPose.orientation.y *= -1;
				rosPose.orientation.z *= -1;
				rosPose.orientation.w *= -1;
		}
		pMsg.pose = rosPose;
		pwc.pose.pose = rosPose;

		pMsg.header.stamp = ros::Time::now();
		pMsg.header.frame_id = Conf().globalFrame;

		pwc.header.stamp = ros::Time::now();
		pwc.header.frame_id = "odom";

		pose_publisher.publish(pMsg);

		// pose_with_covariance_publisher.publish(pwc);

		// tf::Transform transform;
		// transform.setOrigin(tf::Vector3(T(0), T(1), T(2)));
		// tf::Quaternion q;
		// tf::quaternionEigenToTF(quaternion, q);
		// transform.setRotation(q);
		//
		// TB.sendTransform(tf::StampedTransform(transform.inverse(), ros::Time::now(),
		//                                       Conf().lsdFrame, Conf().globalFrame));
}

void ROSOutput3DWrapper::publishDebugInfo(Eigen::Matrix<float, 20, 1> data) {
		std_msgs::Float32MultiArray msg;
		for (int i = 0; i < 20; i++)
				msg.data.push_back((float)(data[i]));

		debugInfo_publisher.publish(msg);
}

} // namespace lsd_slam
