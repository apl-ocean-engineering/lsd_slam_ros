# LSD-SLAM-ROS
This package contains a ROS interface for interaction with a stereo version of [LSD-SLAM](https://vision.in.tum.de/research/vslam/lsdslam?redirect=1). Please see the LSD-SLAM papers for more information:
  * **LSD-SLAM: Large-Scale Direct Monocular SLAM**, J. Engel, T. Schöps, D. Cremers, ECCV '14  
  * **Semi-Dense Visual Odometry for a Monocular Camera**, J. Engel, J. Sturm, D. Cremers, ICCV '13  

Currently, the package tracks this stereo [lsd-slam](https://github.com/apl-ocean-engineering/lsd-slam) repository on branch `stereo`.

The package can also work in mono mode.

The package has been tested for ROS melodic running on Ubuntu 18.04.  

## Getting started

### Dependencies and Installation
This package is dependent on a number of other (public) non-standard packages:

 * [libactiveobject](https://github.com/apl-ocean-engineering/libactiveobject.git) branch `ros`
 * [g2o_catkin](https://gitlab.com/apl-ocean-engineering/lsd-slam/g2o_catkin.git)
 * [g3log_catkin](https://gitlab.com/apl-ocean-engineering/lsd-slam/g3log_catkin.git)
 * [lsd_slam_msgs](https://gitlab.com/apl-ocean-engineering/lsd-slam/lsd_slam_msgs.git)

The simplest way to download and track these packages is using [wstool](http://wiki.ros.org/wstool):
```console
$ cd <catkin_ws>/src
$ git clone https://gitlab.com/apl-ocean-engineering/lsd_slam_ros.git
$ wstool init . lsd_slam_ros/lsd-slam-ros.rosinstall
$ catkin_make (or catkin build, etc.)
```
*Note*: To install without the GUI, you can use update the lsd-slam-ros-no-gui.rosinstall BUT verify that the GUI is not called upon launch (e.g, param launch_GUI must be false in lsd_slam_stereo.launch).  

Upon build, the package should checkout the git submodules [lsd-slam](https://github.com/apl-ocean-engineering/lsd-slam) and [libvideoio-types](https://github.com/apl-ocean-engineering/libvideoio-types)  

Packages can also be manually cloned by consulting the repository list in the rosinstall file.

### Import notes:
1. The launch file lsd_slam_stereo.launch launches a full image processing pipeline
2. Most of the pipeline parameters (e.g., LSD-SLAM parameters and stereo_image_proc paramaters) are spcified through yaml files (see [Configuration](#configuration) and
[ROS Paramaters](#ros-paramaters)), but note that several pipeline paramaters are specified in the launch file. These include (`development` indicates that incorporation of this feature is still in development):
    * do_stereo: Whether to run stereo or mono LSD_SLAM
    * do_EKF: Whether to launch robot_localization EKF node with LSD-SLAM tracking integration ```development```
    * lsd_slam_params_name: Name of lsd_slam yaml configuration file under params/lsd_slam_config
    * EKF_params_name: Name of EKF yaml configuration file under params/ekf_config if EKF is specified
    * Whether to launch a stereo odom node (specifically [this package](https://github.com/apl-ocean-engineering/stereo_visual_odom)) ```development```
    * launch_GUI: To launch the [pangolin GUI](https://gitlab.com/apl-ocean-engineering/lsd_slam_pangolin_gui_ros) package
    * broadcast_info: To optionally launch ros calibration yaml files if camera_info stream is not provided.
    * camera_ns: Base namespace of the incoming image stream
    * flip: flip input images
3. The package assumes synced /camera/<left/right>/camera_info and /camera/<left/right>/image topics. You can change the base namesapce through the camera_ns launch param. If you do not have synced data *TODO*: Add helper nodes documentation
4. LSD-SLAM will start as soon as data is available. Optionally, the param run_on_start in the configuration yaml file can be set to 'false', which will require a service call to start the node.

### Configuration
Most configuration parameters can be found under 'params/. Specifically:
* params for lsd_slam (which include e.g., image gradient promotion threshold parameters and stereo processing paramters) are located under params/lsd_slam_config/
* params/camera_calbrations/ contains yaml files for calibrated camera pairs. If cameras do not natively stream info topics, they must be broadcast separately, and the helper node info_pub can assist.
* params/ekf_config/ contains configuration paramaters for robot_localization, if needed.

See [ROS Paramaters](#ros-paramaters) for details.


### Quick start
The launch file lsd_slam_stereo.launch is configured to run stereo lsd_slam. The pipeline expects input topics of:
* /camera/left/image_raw  
* /camera/left/camera_info  
* /camera/right/image_raw  
* /camera/right/camera_info    

The namespace 'camera' is configurable via the 'camera_ns' paramter in the launch file.

Additionally, input image and decimation size must be specified in the tracked yaml file in params/lsd_slam_config/<file.yaml>
**NOTE**: The decimated image size must be divisible by 32 to work with the image pyramid processing

#### Startup
```console
$ cd <catkin_ws>
$ source devel/setup.bash
$ roslaunch lsd_slam_ros lsd_slam_stereo.launch
```

The processing pipeline should be running and waiting for input images and info.
### Bag files, timestamp synchronization, and debugging
Input topics must be approximately synced throughout the pipeline. In ROS, bag files default to playing on recorded timestamp time. When running bagfiles, verify that sim time is set to true and --clock is publishing:
```console
$ rosparam set /use_sim_time true
$ rosbag play --clock <bagfile>
```

If the node still isn't running, first verify that the input image stack is running properly.
1. Verify that <camera_ns>/disparity is publishing at frame rate. if not, likely a topic error in crop decimate or stereo image proc
2. Verify all the input topics to lsd_slam are running at speed (check topics by running $ rosnode info /lsd_slam).
3. Verify the input image size is equal to the specified camera size divided by the downsample amount in the <yamlfile> file.

If all of these are correct, may be a bug.

## Processing Pipeline
![Screenshot](readme_imgs/LSD-SLAM-Pipeline.png?raw=true "Processing pipeline")

The processing pipeline consists of a series of cascading ROS nodes. In addition to the packaged nodes, this pipeline utilizes [crop_decimate](http://wiki.ros.org/image_proc#image_proc.2BAC8-diamondback.image_proc.2BAC8-crop_decimate) and [stereo_image_proc](https://www.google.com/search?client=ubuntu&channel=fs&q=stereo+image+proc&ie=utf-8&oe=utf-8) to handle image resizing and stereo processing, when necessary.  It also uses [robot_localization](http://docs.ros.org/melodic/api/robot_localization/html/index.html) when asked, for state estimation.

## Core pre-SLAM nodes
* [crop_decimate](http://wiki.ros.org/image_proc#image_proc.2Fdiamondback.image_proc.2Fcrop_decimate):  
  * Left/right input images are decimated by amount specified by the rosparam ```input_image/decimation```. If decimation is set as 1, this node does not run
* [stereo_image_proc](http://wiki.ros.org/stereo_image_proc):
  * ROS stereo processing node. Rectifies the images via the calibration parameters and constructs a disparity map which can be reconstructed as a pointcloud. In mono SLAM mode, this node does not run.
* [robot_localization](http://docs.ros.org/melodic/api/robot_localization/html/index.html) (usage in development):  
  * ROS n-input EKF node. Incorporated into the LSD-SLAM tracking update step. Use is optional.     
* disparity_idepth (custom node)
  * Simple node to construct stereo_image_proc disparity output to inverse depth map.

A working launch file to run these cascading nodes can be seen in launch/lsd_slam_stereo.launch.


## LSD_node
The main node in the package is the LSD_node, which has several required and optional input streams. Note that these are expected by the LSD-SLAM node, not the full image processing pipeline, which is detailed above.
### Subscribed topcis:
* /lsd_slam/left/image_raw([sensor_msgs/Image](http://docs.ros.org/melodic/api/sensor_msgs/html/msg/Image.html))    
  * Input image REQUIRED
* /lsd_slam/right/image_raw([sensor_msgs/Image](http://docs.ros.org/melodic/api/sensor_msgs/html/msg/Image.html))    
  * Input image REQUIRED IF stereo LSD-SLAM is desired
* /lsd_slam/left/camera_info([sensor_msgs/CameraInfo](http://docs.ros.org/melodic/api/sensor_msgs/html/msg/CameraInfo.html))  
   * Left input camera info REQUIRED
* /lsd_slam/right/camera_info([sensor_msgs/CameraInfo](http://docs.ros.org/melodic/api/sensor_msgs/html/msg/CameraInfo.html))  
   * Right input camera info REQUIRED IF stereo LSD-SLAM is desired
* /lsd_slam/disparityIdepth([lsd_slam_msgs/disparityIdepthMsg](https://gitlab.com/apl-ocean-engineering/lsd-slam/lsd_slam_msgs/-/blob/master/msg/disparityIdepthMsg.msg))  
    * Inverse depth map for pixels in reference image (default to left image in stereo) REQUIRED IF stereo LSD-SLAM is desired
* /lsd_slam/odometry([nav_msgs/Odometry](http://docs.ros.org/melodic/api/nav_msgs/html/msg/Odometry.html))
    * Odometry input to help tracking REQUIRED IF EKF fusion is desired and specified


### Published
* /lsd_slam/pointcloud([sensor_msgs/PointCloud2](http://docs.ros.org/api/sensor_msgs/html/msg/PointCloud2.html))  
    * Pointcloud for the current keyframe, in the frame point of view  
* /lsd_slam/pose([geometry_msgs/Pose](http://docs.ros.org/lunar/api/geometry_msgs/html/msg/Pose.html))  
    * Current pose of camera system
* /lsd_slam/debug([std_msgs/Float32MultiArray](http://docs.ros.org/melodic/api/std_msgs/html/msg/Float32MultiArray.html))
    * TODO: STALE
* /lsd_slam/debug_image([sensor_msgs/Image](http://docs.ros.org/melodic/api/sensor_msgs/html/msg/Image.html))
    * Output debug image. Currently set to display high gradient regions
* /lsd_slam/framePoses([lsd_slam_msgs/framePoseData](https://gitlab.com/apl-ocean-engineering/lsd-slam/lsd_slam_msgs/-/blob/master/msg/framePoseData.msg))
    * Transformations between current keyframes and inital world frame. Stored as [qx, qy, qz, qw, x, y, z]   
* /lsd_slam/frames([lsd_slam_msgs/frameMsg](https://gitlab.com/apl-ocean-engineering/lsd-slam/lsd_slam_msgs/-/blob/master/msg/frameMsg.msg))
   * Message for a frame, containing the keyframe pointcloud and keyframe info
* /lsd_slam/graph([lsd_slam_msgs/keyframeGraphMsg](https://gitlab.com/apl-ocean-engineering/lsd-slam/lsd_slam_msgs/-/blob/master/msg/keyframeGraphMsg.msg)
   * Full keyfram graph info, published after each loop closure
* /lsd_slam/twist([geometry_msgs/TwistWithCovarianceStamped](http://docs.ros.org/melodic/api/geometry_msgs/html/msg/TwistWithCovarianceStamped.html))
  * Current frame to frame heading estimate based on
* /set_pose([geometry_msgs/PoseWithCovarianceStamped](http://docs.ros.org/melodic/api/geometry_msgs/html/msg/PoseWithCovarianceStamped.html))
  * Pose estimate published at loop_closure IF rosparam publish_set_pose is true.

### Services:
* /lsd_slam/full_reset([std_srvs/SetBool](http://docs.ros.org/melodic/api/std_srvs/html/srv/SetBool.html))
  * Reset lsd-slam. Will cause system to re-initialize
* /lsd_slam/start([std_srvs/SetBool](http://docs.ros.org/melodic/api/std_srvs/html/srv/SetBool.html))
  * IF run_on_start is false, this service call will begin the SLAM process
* /lsd_slam/stop([std_srvs/SetBool](http://docs.ros.org/melodic/api/std_srvs/html/srv/SetBool.html))
  * Stop running SLAM by not accepting new images, will NOT reset


### ROS Paramaters:
**NOTE**: Most of these paramaters are specified in a yaml file under params/lsd_slam_config  
`dynamic reconfigure` doenotes they can be specified after launch via dynamic reconfigure.
 **TODO** more testing to verify these are all working properly
* LSD_SLAM_frame (string): **TODO**: Rename and verify this is consistent in output wrapper
  * Output frame name
  * default: camera/left
* global_frame (string):
  * Global frame name
  * Defulat: map  
* run_on_start (bool)
  * Run SLAM on node start or wait until service call /lsd_slam/start is called.
  * Default: false
* use_Ekf (bool):
  * Whether to use external odometry estimate or not
  * default: false
* publish_set_pose (bool) `dynamic reconfigure`
  * Publish output topic on /set_pose of current pose estimate on loop_closure. To work with robot_localization
  * Default: false    
* do_left_right_stereo (bool) **TODO** This is deactivated at the moment
  * Propagate the right image into the SLAM processing step
  * Default: false
* /stereo_extrinsics/R/data
  * If do_left_right_stereo is true, rotation matrix setting
  * Default: Flattened 3X3 Identity Matrix
* /stereo_extrinsics/t/data
  * If do_left_right_stereo is true, translation vector setting
  * Default: [3X1] zero translation vector
* input_image/decimation (int):
  * Input decimation amount
  * Default: 1
* input_image/width (int):
  * Pre-decimation width
  * Default: 1920
* input_image/height (int):
  * Pre-decimation height
  * Default: 1024
* lsd_slam_processing/reference_image (int)
  * Reference image number (0 for left, 1 for right) in stereo SLAM mode
  * Default: 0
* lsd_slam_processing/min_virtual_baseline_length (double) `dynamic reconfigure`
  * Minimum temporal baseline to promote keyframe
  * Default: 0.1
* lsd_slam_processing/suppress_LSD_points (bool)
  * Whether to stop propagation of gradient-based LSD points to rely entirely on disparity map input
  * Default: false  
* lsd_slam_processing/sync_disparity_image (bool)
  * To sync (and expected approx. timestamp synced inputs) reference image to disparity image
  * Default: true   
* lsd_slam_debugging/display_input_image (bool) `dynamic reconfigure`
  * Input image to the SLAM system
  * Default: false
* lsd_slam_debugging/display_depth_map (bool) `dynamic reconfigure`
  * Display depth map for debugging purposes
  * Default: false
* lsd_slam_debugging/display_gradient_map (bool) `dynamic reconfigure`
  * Display LSD-SLAM gradient map for debugging purposes
  * Default: false  
* lsd_slam_debugging/display_input_fused_image (bool) `dynamic reconfigure`
  * Display overlayed disparity map on input image
  * Default: false  
* lsd_slam_debugging/print_optimization_info (bool) `dynamic reconfigure`
  * Print to console information about SLAM optimization
  * Default: true  
* lsd_slam_debugging/print_constraint_info (bool) `dynamic reconfigure`
  * Print to console information about SLAM constraints
  * Default: true  
* lsd_slam_debugging/print_threading_info (bool) `dynamic reconfigure`
  * Print to console information about SLAM threading
  * Default: true  
* slam_image_processing/image_saturation (bool) `dynamic reconfigure`
  * Whether to saturate input image
  * Default: false  
* slam_image_processing/saturation_alpha (double) `dynamic reconfigure`
  * If saturation is specified, the alpha parameter in [cv Mat convertTo](https://docs.opencv.org/trunk/d3/d63/classcv_1_1Mat.html#adf88c60c5b4980e05bb556080916978b)
  * Default: 1.0
* slam_image_processing/saturation_beta(int) `dynamic reconfigure`
  * If saturation is specified, the beta parameter in [cv Mat convertTo](https://docs.opencv.org/trunk/d3/d63/classcv_1_1Mat.html#adf88c60c5b4980e05bb556080916978b)
  * Default: 1      

### Helper Nodes
The directory helper_nodes/ contains a small set of python ROS nodes to support SLAM processing, but are not typically critical to the pipeline. The two most important are:
info_pub.py and image_sync_resize.py

1. info_pub.py: When info topics are not available, the info_pub node can be used to publish streams for a left and right camera. See usage in lsd_slam_stereo.launch for example. The node expects the ros param's left_info and right_info to be set in the lsd slam yaml configuration file, which point to a configuration yaml file under params/camera_calibrations/. Note this this publishing will be in live time, which may cause issues when packages require timestamp synced inputs and playback is from a bagfile. --use_sim_time may work, otherwise utilize image_sync_resize.
2. image_sync_resize.py: Basic node which subscribes to incoming image streams and syncs to info topic. Will also resize image, but this is not wise as calibration paramaters will be impacted.

In lsd_slam_stereo.launch, both of these nodes are often used when a bag file only has image topics (by turning broadcast_info to true). However, in that case, the pipeline now expects input image topics to be /camera/left/image_raw_lag to indicate the images are no longer truly live. **TODO**: Rename?     
