## Basic Dockerfile which builds lsd-slam-ros.
#
# Entrypoint is a shell prompt, for now

FROM ros:melodic-perception-bionic

RUN apt-get update && apt-get install -y \
      python-catkin-tools \
      ros-melodic-tf-conversions \
      libglew-dev\
      libsuitesparse-dev \
      libglm-dev python-wstool \
    && rm -rf /var/lib/apt/lists/*

## Create user "lsdslam"
RUN adduser --uid ${user_uid:-1000} --home /home/lsdslam/ lsdslam

## Drop privileges
USER lsdslam

RUN mkdir -p /home/lsdslam/slam_ws/src
COPY --chown=lsdslam:lsdslam . /home/lsdslam/slam_ws/src/lsd-slam-ros

WORKDIR /home/lsdslam/slam_ws
RUN wstool init src/ src/lsd-slam-ros/lsd-slam-ros-no-gui.rosinstall

RUN . /opt/ros/melodic/setup.sh && catkin init && catkin build

RUN echo "source /home/lsdslam/slam_ws/devel/setup.bash" >> /home/lsdslam/.bashrc
ENTRYPOINT "/bin/bash"
