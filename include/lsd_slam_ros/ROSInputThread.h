#pragma once

#include "IOWrapper/OutputIOWrapper.h"

#include "nav_msgs/Odometry.h"

#include "util/ThreadMutexObject.h"

#include "SlamSystem.h"
#include "sensor_msgs/CameraInfo.h"

#include "libvideoio/types/Camera.h"
#include "util/globalFuncs.h"

#include "slam_msgs/disparityIdepthMsg.h"

#include "ROSOutput3DWrapper.h"

#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <ros/ros.h>

#include "std_srvs/SetBool.h"

#include <dynamic_reconfigure/server.h>
#include <lsd_slam_ros/lsdSlamConfig.h>

namespace lsd_slam {

class ROSInputThread {
public:
ROSInputThread(std::shared_ptr<lsd_slam::SlamSystem> &system,
               std::shared_ptr<lsd_slam::ROSOutput3DWrapper> &outputWrapper,
               int refFrame, bool runOnStart = true);
ROSInputThread(std::shared_ptr<lsd_slam::SlamSystem> &system,
               std::shared_ptr<lsd_slam::ROSOutput3DWrapper> &outputWrapper,
               int refFrame, Sophus::SE3d extrinsics, bool runOnStart = true);

~ROSInputThread();

void
setIOOutputWrapper(const std::shared_ptr<lsd_slam::OutputIOWrapper> &out);

cv::Mat parseImage(const sensor_msgs::ImageConstPtr &msg);

// Entry point for boost::thread
void operator()();

std::shared_ptr<lsd_slam::SlamSystem> &system;
std::shared_ptr<lsd_slam::ROSOutput3DWrapper> outputWrapper;

ThreadMutexObject<bool> inputDone;
ThreadSynchronizer inputReady;

// ROS callback
void imageCallback(const sensor_msgs::ImageConstPtr &msg, std::string camera);
void
imageSyncCallback(const sensor_msgs::ImageConstPtr &imgL,
                  const slam_msgs::disparityIdepthMsgConstPtr &disparity);
void cameraInfoCallback(const sensor_msgs::CameraInfoConstPtr &msg,
                        std::string camera);
void disparityMapCallback(const slam_msgs::disparityIdepthMsg::ConstPtr &msg);

void odometryCallback(const nav_msgs::Odometry::ConstPtr &msg);

void SLCallback(const sensor_msgs::PointCloud2ConstPtr &msg);

bool fullReset(std_srvs::SetBool::Request &req,
               std_srvs::SetBool::Response &res);

bool startSlam(std_srvs::SetBool::Request &req,
               std_srvs::SetBool::Response &res);

bool stopSlam(std_srvs::SetBool::Request &req,
              std_srvs::SetBool::Response &res);

void dynamicReconfigureCallback(lsd_slam_ros::lsdSlamConfig &config,
                                uint32_t level);

private:
void cfgOutput(std::string name, double val1, double val2);
void cfgOutput(std::string name, int val1, int val2);
void cfgOutput(std::string name, bool val1, bool val2);
// Setup basic ROS handeling
sensor_msgs::CameraInfo CameraInfo;
cv::Mat callbackImage;
// cv::Mat disparityMap;
std::vector<cv::Mat> imageVector;
libvideoio::Camera _camera;
std::vector<libvideoio::Camera> infoVector;

std::vector<uint8_t> disparityMapIdepthValid;
std::vector<float> disparityMapIdepth;
float iDepthMean;

Eigen::Matrix3f recitifcationMatrix;

Eigen::Vector3f odomLinearTwist;
Eigen::Vector3f odomAngularTwist;
bool odomSet = false;
bool first_odom = true;

bool flip = true;

int refFrame;

Sophus::SE3d extrinsics;

bool newImage;

bool runOnStart;

bool serviceStart;

bool serviceStop;

ros::Time prev_time;

ros::Publisher input_odom_pub;
ros::NodeHandle nh_;


protected:
std::shared_ptr<lsd_slam::OutputIOWrapper> output;
};
} // namespace lsd_slam
