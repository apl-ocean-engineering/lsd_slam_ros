/**
 * This file is part of LSD-SLAM.
 *
 * Copyright 2013 Jakob Engel <engelj at in dot tum dot de> (Technical
 * University of Munich) For more information see
 * <http://vision.in.tum.de/lsdslam>
 *
 * LSD-SLAM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LSD-SLAM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LSD-SLAM. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "IOWrapper/OutputIOWrapper.h"

#include <ros/ros.h>

#include "sensor_msgs/Image.h"
#include "slam_msgs/frameMsg.h"
#include "slam_msgs/framePoseData.h"
#include "slam_msgs/keyframeGraphMsg.h"

#include "std_msgs/Header.h"

#include <pcl/common/transforms.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>

#include "tf/transform_datatypes.h"
#include "tf_conversions/tf_eigen.h"
#include <tf/transform_broadcaster.h>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloud;

namespace lsd_slam {

class Frame;
class KeyFrameGraph;

struct InputPointDense {
		float idepth;
		float idepth_var;
		unsigned char color[4];
};

struct GraphConstraint {
		int from;
		int to;
		float err;
};

struct GraphFramePose {
		int id;
		float camToWorld[7];
};

/** Addition to LiveSLAMWrapper for ROS interoperability. */
class ROSOutput3DWrapper : public OutputIOWrapper {
public:
// initializes cam-calib independent stuff
ROSOutput3DWrapper();
~ROSOutput3DWrapper();

virtual void publishPose(const Sophus::Sim3f &pose);

virtual void
publishKeyframeGraph(const std::shared_ptr<KeyFrameGraph> &graph);

virtual void
publishStateEstimation( const Sophus::Sim3f &pose,
                        const Sophus::SE3f &motion,
                        double timeElapsed);

virtual void
publishStateEstimation(const Sophus::Sim3f &pose,
                       const Eigen::Matrix<float, 6, 1> motion,
                       const Eigen::Matrix<float, 6, 1> acceleration){
}

// Publish pointcloud from graph
virtual void publishPointCloud(const Frame::SharedPtr &kf);

virtual void publishTwist(const Sophus::SE3d &twist) {
}

// publishes a keyframe. if that frame already existis, it is overwritten,
// otherwise it is added.
virtual void publishKeyframe(const Frame::SharedPtr &kf);
virtual void publishFrame(const Frame::SharedPtr &kf,
                          const Eigen::MatrixXf G) {
}

virtual void publishOptimizedPoseEstimate(const Sophus::Sim3f &pose);

virtual void publishFrame(const Frame::SharedPtr &kf, const Eigen::MatrixXf G,
                          const DepthMap::SharedPtr &depthMap);

virtual void updateDepthImage(unsigned char *data);

// published a tracked frame that did not become a keyframe (i.e. has no depth
// data)
virtual void publishTrackedFrame(const Frame::SharedPtr &kf);
virtual void publishTrackedFrame(const Frame::SharedPtr &frame,
                                 const Frame::SharedPtr &kf,
                                 const Eigen::MatrixXf G,
                                 const DepthMap::SharedPtr &depthMap,
                                 SE3 &frameToParentEstimate);

// publishes graph and all constraints, as well as updated KF poses.
virtual void
publishTrajectory(std::vector<Eigen::Matrix<float, 3, 1> > trajectory,
                  std::string identifier);

virtual void publishTrajectoryIncrement(Eigen::Matrix<float, 3, 1> pt,
                                        std::string identifier);

virtual void publishDebugInfo(Eigen::Matrix<float, 20, 1> data);

virtual void keyFrameCallback(const slam_msgs::keyframeMsg::ConstPtr &kfMsg);

virtual void updateFrameNumber(int) {
		;
}

virtual void updateLiveImage(const cv::Mat &img) {
		;
}

int publishLvl;

private:
ros::NodeHandle nh_;
ros::Subscriber sub = nh_.subscribe(
		"/lsd_slam/keyframes", 1, &ROSOutput3DWrapper::keyFrameCallback, this);

std::string liveframe_channel;
ros::Publisher liveframe_publisher;

std::string frame_channel;
ros::Publisher frame_publisher;

std::string current_frame_channel;
ros::Publisher current_frame_publisher;

std::string keyframe_channel;
ros::Publisher keyframe_publisher;

std::string graph_channel;
ros::Publisher graph_publisher;

std::string frame_pose_data_channel;
ros::Publisher frame_pose_data_publisher;

std::string debugInfo_channel;
ros::Publisher debugInfo_publisher;

std::string pose_channel;
ros::Publisher pose_publisher;

std::string pose_with_covariance_channel;
ros::Publisher pose_with_covariance_publisher;

std::string twist_channel;
ros::Publisher twist_publisher;

std::string odom_channel;
ros::Publisher odom_publisher;

std::string pointcloud_channel;
ros::Publisher pointcloud_publisher;

std::string image_channel;
ros::Publisher image_pub;

PointCloud fullPointCloud;
tf::TransformBroadcaster TB;
};
} // namespace lsd_slam
