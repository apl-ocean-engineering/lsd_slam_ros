#include "util/settings.h"
#include <boost/thread.hpp>
#include <fstream>

#include "ros/ros.h"
#include "sensor_msgs/Image.h"
#include "slam_msgs/disparityIdepthMsg.h"
#include "stereo_msgs/DisparityImage.h"

#include "opencv2/core/utility.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <opencv2/photo.hpp>

#include "geometry_msgs/Point32.h"
#include "sensor_msgs/CameraInfo.h"
#include "sensor_msgs/PointCloud.h"

#include "Eigen/Core"
#include "Eigen/Geometry"

#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>

using namespace cv;

typedef pcl::PointCloud<pcl::PointXYZ> PointCloudI;
typedef pcl::PointXYZ PointT;

class DisparityMapDownsample {
  ros::NodeHandle nh_;
  std::string disparityImage_channel;
  ros::Publisher disparity_publisher;
  ros::Publisher image_publisher;
  ros::Publisher pcPub;

  float f;
  Eigen::Matrix3f K;
  cv::Mat img;

  float T;
  float iDepthMean;
  float maxDepth;

  bool debugPC;

  int downsampleX;
  int downsampleY;

public:
  DisparityMapDownsample(int _downsample, bool _debugPC);
  void imageSyncCallback(const stereo_msgs::DisparityImageConstPtr &msg1,
                         const sensor_msgs::ImageConstPtr &msg);
  void disparityImageCallback(const stereo_msgs::DisparityImageConstPtr &msg);
  void infoCallback(const sensor_msgs::CameraInfoConstPtr &msg);
};
